
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Technical Code & Links</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" 
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.3/examples/sign-in/">
 <link rel="stylesheet" type="text/css" href="https://getbootstrap.com/docs/5.3/examples/dashboard/dashboard.css">
    

    

<link href="/docs/5.3/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
<style>
.col-1{
    background: blueviolet;
    /* background: var(--secondary-color); */
    margin:4px ;
}

.col-3{
  background: blueviolet;
 margin-left: 20px;
 margin-top: 10px;
  width: 260px;
  height: 100px;
}

</style>
</head>
<body id="page-top">
        <!-- Content Wrapper -->
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" 
    crossorigin="anonymous"></script>


<div class="col-lg-12 mb-4">
                           <div class="card shadow mb-4">
                          <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Tech Article</h6> 
                                </div>
                               
                                <div class="card-body">
                           <div class="album py-5 bg-light">
                              <div class="container">
                                 <div class="row">
                                 <div class="col-md-12">
          <!-- <div class="card mb-4 shadow-sm"> -->
            <!-- <div class="card-body"> -->
              <!-- <h4 class="card-text"><u></u></h4> -->
              <!-- <div class="d-flex justify-content-between align-items-center"> -->
                              <div class="container">
                                <div class="row">
                               <iframe src="<?=base_url().'assets/pdf/the_lady_of_the_lake.pdf' ?>" width="250" height="700" 
                               frameborder='0' allowfullscreen>
                              </iframe>
                                <!-- </div> -->
                              <!-- </div> -->
                         <!-- </div> -->
                        </div></div></div>
                        <div class="col-md-12">
          <!-- <div class="card mb-4 shadow-sm"> -->
            <!-- <div class="card-body"> -->
              <!-- <h4 class="card-text"><u></u></h4> -->
              <!-- <div class="d-flex justify-content-between align-items-center"> -->
                              <div class="container">
                                <div class="row">
                                <iframe src="<?=base_url().'assets/pdf/the_wedding_date.pdf' ?>" width="250" height="700"></iframe>
</div>
                                <!-- </div> -->
                              <!-- </div> -->
                         <!-- </div> -->
                        </div></div></div>
            <div class="col-md-12">
          <!-- <div class="card mb-4 shadow-sm"> -->
            <!-- <div class="card-body"> -->
              <!-- <div class="d-flex justify-content-between align-items-center"> -->
              <div class="container">
                                <div class="row">
                                <iframe src="<?=base_url().'assets/pdf/THE-ALCHEMIST.pdf' ?>" width="250" height="700"></iframe>
                                <!-- </div> -->
                              <!-- </div> -->
                                    <!-- </div> -->
                                 </div></div></div>
                                  
                                    <div class="col-md-12">
          <!-- <div class="card mb-4 shadow-sm"> -->
            <!-- <div class="card-body"> -->
              <!-- <div class="d-flex justify-content-between align-items-center"> -->
              <div class="container">
                                <div class="row">
                                <iframe src="<?=base_url().'assets/pdf/the_lady_of_the_lake.pdf' ?>" width="250" height="700"></iframe>
                                <!-- </div> -->
                              <!-- </div> -->
                                    <!-- </div>  -->
                                </div></div></div>
                                <div class="container">
                                <div class="row">
                              
                                <div class="col-3" >
      <div class="p-5" class="btn btn-primary" data-bs-toggle="collapse" data-bs-target="#collapseWidthExample" 
     aria-expanded="false" aria-controls="collapseWidthExample" >
      <h6 style="color:white;" class="gtl"><center>AI/ML/Robotics/Data Science</center></h6></div> 
      <div style="min-height: 220px;">
  <div class="collapse collapse-horizontal" id="collapseWidthExample">
    <div class="card card-body " style="width: 250px;">
    Data Science is a broader field, but all are part of the AI family.Data scientists who work in AI and robotics 
    R&D aren't just building better machines, they're also building better data science
    <a href="<?=base_url() ?>Welcome/techDomain/ai" >Know More</a>
    </div>
  </div>
</div>
    </div>
    <div class="col-3" >
      <div class="p-5" class="btn btn-primary" data-bs-toggle="collapse" data-bs-target="#collapseWidthExample" 
     aria-expanded="false" aria-controls="collapseWidthExample" >
      <h6 style="color:white;" class="gtl"><center>Microservices</center></h6></div> 
      <div style="min-height: 220px;">
  <div class="collapse collapse-horizontal" id="collapseWidthExample">
    <div class="card card-body " style="width: 250px;">
    Microservices are an architectural and organizational approach to software development where software is 
    composed of small independent services that communicate over well-defined APIs. 
    <a href="<?=base_url() ?>Welcome/techDomain/mi" >Know More</a>
    </div>
  </div>
</div>
    </div>
    <div class="col-3" >
      <div class="p-5" class="btn btn-primary" data-bs-toggle="collapse" data-bs-target="#collapseWidthExample" 
     aria-expanded="false" aria-controls="collapseWidthExample" >
      <h6 style="color:white;" class="gtl"><center>Android/IOS Development</center></h6></div> 
      <div style="min-height: 220px;">
  <div class="collapse collapse-horizontal" id="collapseWidthExample">
    <div class="card card-body " style="width: 250px;">
    Apple iOS developers use Swift, Apple's standard programming language, which typically requires less code and is considered easier to use. 
    Android platform developers use Java and Kotlin, which are typically more time-consuming and cumbersome.
    <a href="<?=base_url() ?>Welcome/techDomain/an" >Know More</a>
    </div>
  </div>
</div>
    </div>
    <div class="col-3" >
      <div class="p-5" class="btn btn-primary" data-bs-toggle="collapse" data-bs-target="#collapseWidthExample" 
     aria-expanded="false" aria-controls="collapseWidthExample" >
      <h6 style="color:white;" class="gtl"><center>DevOps Engineer</center></h6></div> 
      <div style="min-height: 220px;">
  <div class="collapse collapse-horizontal" id="collapseWidthExample">
    <div class="card card-body " style="width: 250px;">
    A DevOps engineer is an IT professional who works with software developers, 
    system operators (SysOps) and other production IT staff to oversee code releases .
      <a href="<?=base_url() ?>Welcome/techDomain/de" >Know More</a>
    </div>
  </div>
</div>
    </div>
                                </div></div>
        </body>
</html>