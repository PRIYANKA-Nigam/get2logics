
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Jira Blogs</title>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" 
    integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" 
    crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" 
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
        <style>
        body{
	font-family:Verdana, Geneva, sans-serif;
	font-size:18px;
	background-color:#CCC;
}

.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.fas{
	margin-top:18px;
	font-size: 2em;
}
#icon{
  width: 30px;
  cursor: pointer;
}
:root{
  --primary-color:#edf2fc;
  --secondary-color:white;
}
.dark-theme{
  --primary-color:yellow;
  --secondary-color:red;
}
.g{
  background: var(--primary-color);
}
.gtl{
  background: var(--secondary-color);
}
body{
  background: var(--primary-color);
}
      </style>
</head>

<body id="page-top">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" 
    crossorigin="anonymous"></script>
    <div class="container">
    <div id="google_element" style="float:right"></div>
  <script src="https://translate.google.com/translate_a/element.js?cb=loadGoogleTranslate"></script>
        <script>
           function loadGoogleTranslate(){
            new google.translate.TranslateElement("google_element");
           }
            </script>
             <img src='<?=base_url().'setting.jpg'?>' id="icon" width="25px" height="25px" >
    <a href="<?=base_url().'Welcome/Blog'?>" ><button class="btn btn-success" >Back</button></a>
        <h1>JIRA </h1><hr><br>
    <div class="row">

<!-- Earnings (Monthly) Card Example -->
<div class="col-md-6">
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body ">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                <div class="h5 mb-0 font-weight-bold text-gray-800">JIRA Overview</div><br>
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 gtl">
                    Jira is a project management software use for software  development  
                    across industries it allows to view ,track & report task/issued over the project  we r working on.<br>

Epic gives an abstract view of our project workflow. It remains in the planning backlog.<br>
Story is in the delivery backlog.<br>
Story point - It estimates measurement of complexity of the story.<br>
Issue -> it can be any task, defect/bug etc which is in the to do list.<br><br>
<b>Create an issue(to make an element for our project workflow)</b><br>
Backlog ->epic->story<br>
<b>Creating sub-task under the issue/story->click->select task</b><br>
<b>DoD (Definition of Done ) -></b> ensures that the working s/w produced at the end of 
the sprint is of high quality & potentially shippable.<br>
<b>Sprint Burn Down -></b> To check how much work remain in a sprint backlog , 
understand how quickly team has completed task , predict when team will 
achieve goal of sprint.<br>
________________________________________<br>

Atlassian created Jira in the year 2002 and it was initially 
built to be an issue and bug tracking application.Now, Jira 
software has grown into an agile project management tool 
    catering to software development teams - small and big</div>
                  
                </div>
               
            </div>
        </div>
    </div>
</div>

<!-- Earnings (Monthly) Card Example -->
<div class="col-md-6 ">
    <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body ">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                <div class="h5 mb-0 font-weight-bold text-gray-800">JIRA's pros and cons</div><br>
                </div>
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1 gtl">
                    <b>PROS :</b>
Jira software lets you add story points to each issue to quantify the work 
required to be done and also group related issues using its Epic issue type
Jira software has agile capabilities that supports frameworks such as scrum 
and kanban with features like scrum and kanban boards.<br>

<b>CONS :</b>
Most developers also hate this app for its slow speed.
Jira’s agile capabilities lack finesse when it comes to implementing 
frameworks like scrum, unlike Zepel. You don’t get the elegant Sprints view 
that has built-in agile reports such as burnup and burndown charts. 
(Burn down and burn up charts are two types of charts that project managers 
use to track and communicate the progress of their projects.<br> A burn down 
chart shows how much work is remaining to be done in the project, whereas 
a burn up shows how much work has been completed, and the total amount of work. 
Burn down or burn-up charts are used to showcase progress of a project) .<br>

Hence, JIRA is functional but not developers' friendly</div>
                   
               
            </div>
        </div>
    </div>
</div>

    </div> <br>
    <div class="row">

<!-- Earnings (Monthly) Card Example -->
<div class="col-md-6">
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body ">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                <div class="h5 mb-0 font-weight-bold text-gray-800">Azure DevOps Vs Jira </div><br>
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 gtl">
                    Azure DevOps and Jira are both helpful tools for software 
                    development teams. Jira uses Agile methodologies, offers additional 
                    search functionality and can be used beyond software development 
                    projects. Azure DevOps can be used by cross-functional teams 
                    throughout the entire lifecycle of a software application.<br>
                <b>Azure DevOps Key functionality : </b><br>
                To collaborate and track software development projects from beginning to end.<br>
                 <b>JIRA key functionality : </b><br>
                 Manage projects for teams using Agile methodologies, such as Scrum.<br>
                 Jira supports Agile methodology, such as Scrum and Kanban, and allows teams 
                 to adjust workflow iterations and add more features while in progress, 
                 which isn’t available with Azure DevOps.<br>

Conversely, Azure DevOps allows teams to view a project from start to finish, 
along with the connections between various stages and work items, but 
Jira doesn’t allow teams to view previous tasks or iterations. For instance, 
users of Jira can’t see whether a completed Story is associated with a 
software release.<br><br>
<b>If you are looking for something to help you manage the entire life 
    cycle of a software application development, from ideation to 
    deployment, Azure DevOps will probably be your best option. If, 
    on the other hand, you want a project development tool to be used 
    for software development, as well as other projects, Jira can better 
    meet your needs.</b></div>
                  
                </div>
                
            </div>
        </div>
    </div>
</div>

<!-- Earnings (Monthly) Card Example -->
<div class="col-md-6 ">
    <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body ">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                <div class="h5 mb-0 font-weight-bold text-gray-800">JIRA Vs Github</div><br>
                </div>
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1 gtl">
                    GitHub is an Internet hosting service for software development 
                    and version control using Git. It provides the distributed version 
                    control of Git plus access control, bug tracking, software feature 
                    requests, task management, continuous integration, and wikis for every project. <br><br>
                    It is a code hosting platform for version control and collaboration. 
                    It lets you and others work together on projects from anywhere.<br>
                    Joining as a recent addition to the list of Jira alternatives is GitHub.
Popularly known for source code management, now, GitHub has expanded its horizons 
from version control systems to project management. And it is rising to fame as a
 popular replacement to Jira as it can do most of what Jira can do.</div>
                   
               
            </div>
        </div>
    </div>
</div>

    </div><br>
    <div class="row">

<!-- Earnings (Monthly) Card Example -->
<div class="col-md-6">
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body ">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                <div class="h5 mb-0 font-weight-bold text-gray-800">Github</div><br>
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 gtl">
                    Joining as a recent addition to the list of Jira alternatives is GitHub.
Popularly known for source code management, now, GitHub has expanded its horizons from 
version control systems to project management. And it is rising to fame as a popular 
replacement to Jira as it can do most of what Jira can do.<br>

<b>PROS</b><br>
GitHub’s project boards have useful templates such as basic kanban, automated 
kanban, automated kanban with triggers for PR review status to help prioritize work and customize workflows.
GitHub’s pricing plan is quite affordable.<br>

<b>CONS</b><br>
Unlike Jira software and Zepel, GitHub lacks powerful agile capabilities other than simple kanban software and Milestones for scrum.
Unlike Atlassian’s Jira, GitHub doesn’t have story points to capture and quantify the work required to be completed.<br><br>

<b>Forking a repository in github -></b><br>
A fork is a copy of a repository that you manage. Forks let you make changes to a project
without affecting the original repository. You can fetch updates from or submit changes 
to the original repository with pull requests.<br>
<b>------–-----------  Forking Vs Branching</b<br><br>
When you fork a repository, you’re creating your own copy of the entire project 
including the repo. Forking in GitHub is duplicating an entire git repository into a 
different account. So if you have the account marvin, and you fork rails/rails, you will have marvin/rails.
A branch is simply a separate set of commits within a repo that already exists.<br>
<b>–-----------------------Github Issues -></b><br>
Issues is a place in github to leave comment about the project.Any developer 
who is visiting my repository can add issue in that.In issue he/she can specify 
anything which he wants us to perform or add in our code.This issue will have a 
unique id or number which the owner can reference in commit message when he is 
resolving that particular issue .So, that the assigner should know that its' 
issue has been fixed.Once the owner resolves the issue ,he can close that issue.</div>
                  
                </div>
               
            </div>
        </div>
    </div>
</div>



    </div>
</div>
<script> 
     var icon=document.getElementById("icon");
     icon.onclick =function(){
      document.body.classList.toggle("dark-theme");
      if(document.body.classList.contains("dark-theme")){
        icon.src="<?=base_url().'setting.jpg'?>";
      }else{
        icon.src="<?=base_url().'moon.jpg'?>";
      }
     }
     </script>  
      <a href="<?= base_url().'Welcome/test3'?>" class="float">
<i class="fas fa-info-circle"></i>
</a>
</body></html>
<!-- End of Main Content -->