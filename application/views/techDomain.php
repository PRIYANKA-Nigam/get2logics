<?php
 $domain = $this->uri->segment(3);
?>
<html>
    <head>
        
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" 
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.3/examples/sign-in/">
 <link rel="stylesheet" type="text/css" href="https://getbootstrap.com/docs/5.3/examples/dashboard/dashboard.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" 
    integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" 
    crossorigin="anonymous" referrerpolicy="no-referrer" />

    

<link href="/docs/5.3/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <style>
        
body{
	font-family:Verdana, Geneva, sans-serif;
	font-size:18px;
	background-color:#CCC;
}

.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.fas{
	margin-top:18px;
	font-size: 2em;
} 
section{
    height: auto;
    width: auto;
    display: inline-block;
    align-items: left;
    margin-top: 25px;
    margin-left: 20px;
  
   
    justify-content: center;
    text-transform: uppercase;
}
#ai{
    background: linear-gradient(-45deg,white 30%,yellow 40%);
}
#mi{
    background: linear-gradient(-45deg,lightblue 30%,yellow 0%);
}
#an{
    background: linear-gradient(-45deg,lightgreen 30%,yellow 0%);
}
#de{
    background: linear-gradient(-45deg,purple 30%,yellow 0%);
}
body{
  background: var(--primary-color);
  max-height: 100vh; 
  /* by giving max-height: 100vh the footer got stick to the bottom of the page */
}
</style>
    </head>
    <body>
    <div id="google_element" style="float:right"></div>
  <script src="https://translate.google.com/translate_a/element.js?cb=loadGoogleTranslate"></script>
        <script>
           function loadGoogleTranslate(){
            new google.translate.TranslateElement("google_element");
           }
            </script>
<label><b><a href="<?=base_url().'Welcome'?>" ><button class="btn btn-success" >Back</button></a></b></label><br>
        <h1 style="background:green;margin:auto;display:block"><center>Tech Domains</center></h1>

    <section id="ai" >
<h1><center>AI/ML/Robotics/Data Science</center></h1><br>
     This is a biggest stream and a future of world.The more you excel in this field the more you grow.The core languages to 
      make career in these fiels are - Python,Java,C++,R, etc. The most popular language for data science is Python.
      Java is also highly scalable for AI/ML data science algorithm.Many big data framework Hadoop,Hive and Spark
      (are also java based) used in ML.Since JVM allows users to create machine learning tools fast and roll them out at speed.<br>
      <b>Data scientists</b> work to extract valuable insights from big data. They use computer programs to collect, 
      clean, structure, analyze and visualize big data. They may also program algorithms to query data for 
      different purposes. <b>Machine learning engineers</b> work with data scientists to develop and maintain scalable
       machine learning software models. <b>AI engineers</b> work closely with data scientists to build deployable versions 
       of the machine learning models.<br>
       <b>Data Science</b><br>

Data scientists focus on collecting, processing, analyzing, visualizing, and making predictions based on data. 
In data science, the focus remains on building models that can extract insights from data. Skills required 
include programming, data visualization, statistics, and coding. Data scientists are instrumental in every 
industry, using their skills to identify medical conditions, optimize logistics, inform city planning, fight 
fraud, improve shopping experiences, and more.<br><br>

<b>Machine Learning</b><br>

Data scientists who work in machine learning make it possible for machines to learn from data and generate accurate 
results. In machine learning, the focus is on enabling machines to easily analyze large sets of data and make 
correct decisions with minimal human intervention. Skills required include statistics, probability, data modeling, 
mathematics, and natural language processing. Machine learning specialists develop applications based on algorithms 
that can detect defects in parts, improve manufacturing processes, streamline inventory and supply chain management, 
prevent crime, and more.<br><br>

<b>Artificial Intelligence</b><br>

Data scientists who specialize in artificial intelligence build models that can emulate human intelligence. AI 
involves the process of learning, reasoning, and self-correction. Skills required include programming, statistics, 
signal processing techniques and model evaluation. AI specialists are behind our options to use AI-powered personal 
assistants and entertainment and social apps, make autonomous vehicles possible and ensure payment technologies are 
safe to use.<br><br>
    </section>
    <section id="mi">

    <h1><center>Microservices</center></h1><br>
    It is an architecture ,an style of  building rapid web based applications. Now a days , it is adopted by industries 
     to build large scale applications that have high customer influx in their sites.Popular language for microservices 
     Development is Java & spring. But many other languages can also be used in such Development like - Python,Golang,Node JS,
     .Net. Microservice style fragment our applications into a series of smaller services , each executing in its own 
     process and interacting with light weight mechanism.<br>
     A Microservices developer is a highly skilled individual who can efficiently build software systems and can also develop 
     low-latency applications for mission-critical business systems.As a microservices developer, you are required to build 
     software systems that have well-defined interfaces.<br>
     <br>
     Microservices are an architectural and organizational approach to software development where software is composed 
     of small independent services that communicate over well-defined APIs. These services are owned by small, 
     self-contained teams.

Microservices architectures make applications easier to scale and faster to develop, enabling innovation and 
accelerating time-to-market for new features.<br>
With a microservices architecture, an application is built as independent components that run each application 
process as a service. These services communicate via a well-defined interface using lightweight APIs. Services 
are built for business capabilities and each service performs a single function. Because they are independently
 run, each service can be updated, deployed, and scaled to meet demand for specific functions of an application.
    </section>
    <section id="an">
<h1><center>Android/IOS Development</center></h1><br>
     There is always a craze in mobile app development. Their are various softwares in market to construct your app in 
     no time and put them in production.Java is widely used well known programming language for android and IOS App development.
     If you are looking for IOS app than Objective-C , Swift could be your best bet.For android Development, Java can be use.However 
     Kotlin is also a preferred language .It is statically typed language used by over 60% of professionals.<br><br>
     A mobile app developer uses programming languages and development skills to create, test, and develop applications 
     on mobile devices. They work in popular operating system environments like iOS and Android and often take into 
     account UI and UX principles when creating applications.<br>
     A mobile app developer is able to create software for phones and tablets, and is familiar with the newest 
     technologies in the mobile world. Mobile development requires staying in the mobile head space, meaning that 
     it’s even more important than with other hardware contexts to optimize performance, battery, network, and 
     memory management. A developer must be also aware of how to deal with device fragmentation, often working 
     closely with a designer to achieve the best user experience (UX) results.<br>

“Mobile app developer” is a very wide term, because it’s not limited to developers who write native code for platforms 
like Android and iOS. It can also include hybrid app developers working with frameworks such as Cordova or Ionic, 
and JavaScript and C# developers, who are using React Native and Xamarin to write mobile apps. These are distinct 
specializations, so it’s crucial to either specify what technology you intend to use in the app or make it clear 
that you are open to technology propositions.<br><br>
<b>Responsibilities</b><br>

Developing new features and user interfaces from wireframe models<br>
Ensuring the best performance and user experience of the application<br>
Fixing bugs and performance problems<br>
Writing clean, readable, and testable code<br>
Cooperating with back-end developers, designers, and the rest of the team to deliver well-architected and high-quality solutions<br><br>

<b>Skills</b><br>
Extensive knowledge about mobile app development. This includes the whole process, from the first line of code to publishing in the store(s)<br>
Deep knowledge of mobile platforms on which the app runs, e.g., Android, iOS, etc. <br>
Proficiency with writing automated tests in {{ JUnit, Espresso, Mocha, Jest, Enzyme, XCTest, etc. 
    depending on the libraries you use to test }}<br>
Familiarity with RESTful APIs and mobile libraries for networking, specifically {{ Retrofit, axios, Alamofire, etc. }}<br>
Familiarity with the JSON format<br>
Experience with profiling and debugging mobile applications<br>
Strong knowledge of architectural patterns—MVP, MVC, MVVM, and Clean Architecture—and the ability to choose the best solution for the app<br>
Familiarity with Git<br>
Familiarity with push notifications<br>
Understanding mobile app design guidelines on each platform and being aware of their differences<br>
Proficiency in { Kotlin/Java/Swift/Objective-C/JavaScript/C#, whichever language you use in the app }<br>
    </section>
    <section id="de">
<h1><center>DevOps Engineer</center></h1><br>
This is also a high paying job field and is good for the users who have less coding background.Here the DevOps 
   engineer works with the development team to tackle the necessary coding and scripting to connect various 
   applications elements, such as APIs,libraries and softaware development Kits(SDKs) and integrate other components 
   such as SQL data management or messaging tools that devOps team needs to run the applications.To have growth in
   this field you need to know concepts like - VM,containers,Docker,Kubernetes,Microservices.CI/CD tools like- jenkins,
   Splunk,Sonar Qube and also must have a little background on cloud computing.And thorough knowledge in any of the one cloud
   azure,aws,gcp.<br><br>
   <b>DevOps is a methodology in the software development and IT industry. Used as a set of practices 
    and tools, DevOps integrates and automates the work of software development and IT operations as 
    a means for improving and shortening the systems development life cycle.</b><br><br>
   DevOps engineers reduce that complexity, closing the gap between actions needed to quickly change an application, 
   and the tasks that maintain its reliability.<br>

Development teams and IT operations teams can have different skills and different goals. Developers want to introduce 
new features to an application, while operations teams want to preserve the stability of an application once it is released. <br>

DevOps is all about the unification and automation of processes, and DevOps engineers are instrumental in combining 
code, application maintenance, and application management. All of these tasks rely on understanding not only development 
life cycles, but DevOps culture, and its philosophy, practices, and tools. <br>

Within an agile environment, developers, system administrators, and programmers can be siloed, working on the same 
product but not sharing information necessary to ensure value to the user. <br>
    </section>
    <?php 
        include('footer.php');
        ?>
          <a href="<?= base_url().'Welcome/test3'?>" class="float">
<i class="fas fa-info-circle"></i>
</a>
    </body>
</html>