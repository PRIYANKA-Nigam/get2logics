
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// session_unset();
//         unset($_SESSION['quiz']);
//         session_destroy();
    

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Quiz Result</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" 
    integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" 
    crossorigin="anonymous" referrerpolicy="no-referrer" />
    <Style>
        body{
	font-family:Verdana, Geneva, sans-serif;
	font-size:18px;
	background-color:#CCC;
}

.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.fas{
	margin-top:18px;
	font-size: 2em;
} 
    </Style>
</head>
<body>
<div id="google_element" style="float:right"></div>
  <script src="https://translate.google.com/translate_a/element.js?cb=loadGoogleTranslate"></script>
        <script>
           function loadGoogleTranslate(){
            new google.translate.TranslateElement("google_element");
           }
            </script>
<div id="container">
	<h1>Quiz Result!</h1>

    <?php  $score=0; ?>

    <?php $array1=array(); ?>
    <?php $array2=array(); ?>
    <?php $array3=array(); ?>
    <?php $array4=array(); ?>
    <?php $array5=array(); ?>
    <?php $array6=array(); ?>
    <?php $array7=array(); ?>
    <?php $array8=array(); ?>


    <?php foreach($checks as $checkans){ ?>
        <?php array_push($array1,$checkans); } ?>
        <?php foreach($results as $res){ ?>
        <?php array_push($array2,$res->answer); 
              array_push($array3,$res->qid); 
              array_push($array4,$res->question); 
              array_push($array5,$res->option1); 
              array_push($array6,$res->option2); 
              array_push($array7,$res->option3); 
              array_push($array8,$res->option4); 
        
    
    }  ?>

    <?php
//echo count($results);
    for($i=0;$i<count($results);$i++){ ?>

    <form method="post" action="<?php echo base_url();?>Welcome/Quiz">

<p><?=$array3[$i]?>.<?=$array4[$i]?></p>

<?php if($array2[$i]!=$array1[$i]){ ?>

    <p><span style="background-color:#ff9c9e"><?=$array1[$i]?></span></p>
    <p><span style="background-color:#adff84"><?=$array2[$i]?></span></p>


<?php }else{ ?>
    <p><span style="background-color:#adff84"><?=$array1[$i]?></span></p>
    <?php $score=$score+1;  ?>
<?php } } ?>
<br><br>

<h2>Your Score: </h2>
<h1><?=$score?>/<?=count($results)?></h1>
<input type="submit" value="Play Again">
    </form>
</div>
     <a href="<?= base_url().'Welcome/test3'?>" class="float">
<i class="fas fa-info-circle"></i>
</a>   
</body>
</html>