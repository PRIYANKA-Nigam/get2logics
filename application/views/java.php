
<!doctype html>
<html lang="en" data-bs-theme="auto">
  <head><script src="/docs/5.3/assets/js/color-modes.js"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.111.3">
    <title>Java Blogs</title>
   
<script src="https://code.jquery.com/jquery-3.6.3.js"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <link rel = "stylesheet" href  = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" 
    integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" 
    crossorigin="anonymous" referrerpolicy="no-referrer" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js" 
integrity="sha512-pumBsjNRGGqkPzKHndZMaAG+bir374sORyzM3uulLV14lN5LyykqNk8eEeUlUkB3U0M4FApyaHraT65ihJhDpQ==" 
crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style3.css');    ?>">
  <style>
   *{padding:0;margin:0;}

body{
	font-family:Verdana, Geneva, sans-serif;
	font-size:18px;
	background-color:#CCC;
}

.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.fas{
	margin-top:18px;
	font-size: 2em;
} 
   .container{
    background: rgba(255, 255  , 255, 0.1);
    backdrop-filter: blur(15px);
    width: 180px;
    height: 100vh;
    position: fixed;
    top: 0;
    left: 0;
    /* overflow-y: auto; */
    transition: 0.6s ease;
    transition-property: left;
}
.container #sidebarMenu{
    width: 100%;
    margin-top: 20px;
}
      label{
    text-align: center;
    position: fixed;
    top: 0;
    left: 35%;
    font-size: 50px;
    /* top:10%; */
    color: white;
}
section{
    height: auto;
    width: auto;
    display:inline-block;
    align-items: left;
    margin-top: 35px;
    margin-left: 250px;
    padding-left: 200px;
    padding-top: 50px;
    justify-content: center;
    text-transform: uppercase;
}
#j1{
    background: linear-gradient(-50deg,white 40%,yellow 40%);
}
#j2{
    background: linear-gradient(-50deg,lightblue 40%,yellow 0%);
}
#j3{
    background: linear-gradient(-45deg,lightgreen 40%,yellow 0%);
}
#j4{
    background: linear-gradient(-45deg,purple 40%,yellow 0%);
}
#ja{
    background: linear-gradient(-50deg,lightblue 40%,yellow 0%);
}
#j5{
    background: linear-gradient(-50deg,crimson 40%,yellow 0%);
}
#j6{
    background: linear-gradient(-50deg,white 40%,yellow 40%);
}
#j7{
    background: linear-gradient(-50deg,lightblue 40%,yellow 0%);
}
#j3{
    background: linear-gradient(-45deg,lightgreen 40%,yellow 0%);
}
#j8{
    background: linear-gradient(-45deg,purple 40%,yellow 0%);
}
#j9{
    background: linear-gradient(-50deg,crimson 40%,yellow 0%);
}
#j10{
    background: linear-gradient(-50deg,white 40%,yellow 40%);
}
#j11{
    background: linear-gradient(-50deg,lightblue 40%,yellow 0%);
}
#j12{
    background: linear-gradient(-45deg,lightgreen 40%,yellow 0%);
}
#j13{
    background: linear-gradient(-45deg,purple 40%,yellow 0%);
}
#j14{
    background: linear-gradient(-50deg,crimson 40%,yellow 0%);
}
#j15{
    background: linear-gradient(-50deg,white 40%,yellow 40%);
}
#j16{
    background: linear-gradient(-50deg,lightblue 40%,yellow 0%);
}
#j17{
    background: linear-gradient(-45deg,lightgreen 40%,yellow 0%);
}
#j18{
    background: linear-gradient(-45deg,purple 40%,yellow 0%);
}
#j19{
    background: linear-gradient(-50deg,crimson 40%,yellow 0%);
}
#j20{
    background: linear-gradient(-50deg,white 40%,yellow 40%);
}
#j21{
    background: linear-gradient(-50deg,lightblue 40%,yellow 0%);
}
#j22{
    background: linear-gradient(-45deg,lightgreen 40%,yellow 0%);
}
#j23{
    background: linear-gradient(-45deg,purple 40%,yellow 0%);
}
#j24{
    background: linear-gradient(-50deg,crimson 40%,yellow 0%);
}
#j25{
    background: linear-gradient(-50deg,white 40%,yellow 40%);
}
#j26{
    background: linear-gradient(-50deg,lightblue 40%,yellow 0%);
}
#j27{
    background: linear-gradient(-45deg,lightgreen 40%,yellow 0%);
}
#j28{
    background: linear-gradient(-45deg,purple 40%,yellow 0%);
}
#j29{
    background: linear-gradient(-50deg,crimson 40%,yellow 0%);
}
#j30{
    background: linear-gradient(-50deg,white 40%,yellow 40%);
}
#j31{
    background: linear-gradient(-50deg,lightblue 40%,yellow 0%);
}
#j32{
    background: linear-gradient(-45deg,lightgreen 40%,yellow 0%);
}
#j33{
    background: linear-gradient(-45deg,purple 40%,yellow 0%);
}
#j34{
    background: linear-gradient(-50deg,crimson 40%,yellow 0%);
}
#j35{
    background: linear-gradient(-50deg,white 40%,yellow 40%);
}
#j36{
    background: linear-gradient(-50deg,lightblue 40%,yellow 0%);
}
#j37{
    background: linear-gradient(-45deg,lightgreen 40%,yellow 0%);
}
#j38{
    background: linear-gradient(-45deg,purple 40%,yellow 0%);
}
#j39{
    background: linear-gradient(-50deg,crimson 40%,yellow 0%);
}
#j40{
    background: linear-gradient(-50deg,white 40%,yellow 40%);
}
#j41{
    background: linear-gradient(-50deg,lightblue 40%,yellow 0%);
}
#j42{
    background: linear-gradient(-45deg,lightgreen 40%,yellow 0%);
}
#j43{
    background: linear-gradient(-45deg,purple 40%,yellow 0%);
}
#j44{
    background: linear-gradient(-50deg,crimson 40%,yellow 0%);
}
#j45{
    background: linear-gradient(-50deg,white 40%,yellow 40%);
}
#j46{
    background: linear-gradient(-50deg,lightblue 40%,yellow 0%);
}
#j47{
    background: linear-gradient(-45deg,lightgreen 40%,yellow 0%);
}
#j48{
    background: linear-gradient(-45deg,purple 40%,yellow 0%);
}
#j50{
    background: linear-gradient(-50deg,crimson 40%,yellow 0%);
}
#j51{
    background: linear-gradient(-50deg,white 40%,yellow 40%);
}
#j52{
    background: linear-gradient(-50deg,lightblue 40%,yellow 0%);
}
#j53{
    background: linear-gradient(-45deg,purple 40%,yellow 0%);
}
#java{
    background: linear-gradient(-45deg,lightgreen 40%,yellow 0%);
}
#sealed{
    background: linear-gradient(-50deg,white 40%,yellow 40%);
}

   </style> 
    <link href="sign-in.css" rel="stylesheet">
  </head>
  <body class="text-center">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" 
    crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" 
integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>  
   
<div class="container" style="margin-top:10px">
  <div class="row">

    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-body-tertiary sidebar collapse">
    <div class="side-bar" >
    <div class="menu">
      <div class="position-sticky pt-3 sidebar-sticky">
      <div id="wrapper">
        <ul class="nav flex-column navbar-nav bg-gradient-primary  sidebar-dark accordion" 
        id="accordionSidebar" style="background:brown">
          <li class="nav-item active">
            <a class="nav-link" aria-current="page" href="#">
            <div class="sidebar-brand-icon rotate-n-15">
                </div>
              <span data-feather="home" class="align-text-bottom">
           Java</span>
            </a>
          </li>
          <li>
          <div class="item">
             <a class="sub-btn" href="#j1"><i class="fas fa-info-circle"></i>What is Java.<i class="fas fa-angle-right dropdown"></i></a>
            <div class="sub-menu">
                <a href="#j2" class="sub-item">Datatypes</a>
                <a href="#j3" class="sub-item">Reserved Keywords</a>
                <a href="#j4" class="sub-item">Exceptions/Errors</a>
                <a href="#j5" class="sub-item">Generics</a>
                <a href="#j6" class="sub-item">Abstract classes vs Interface</a>
                <a href="#j7" class="sub-item">Super/this keyword</a>
                <a href="#j8" class="sub-item">Static/non-static members</a>
                <a href="#j9" class="sub-item">Superclass members</a>
                <a href="#j10" class="sub-item">JDK/JRE/JVM/JIT</a>
            </div>
            </div>
          </li>
          <hr class="sidebar-divider">
  <div style="margin:auto">
            <div class="dropdown open">
                <p class="border-none outline-none "  id="triggerId"
                data-aria-expanded="true">
                <i class="fas fa-info-circle"></i>
                <span class="d-none d-sm-inline"><a class="dropdown-item" href="#j11">Collections Framework</a></span></p>
            <div class="dropdown-menu" aria-labelledby="triggerId">
                <a class="dropdown-item" href="#j12">Concurrent Collection</a>
                <a class="dropdown-item" href="#j13">Traditional Collection</a>
            </div> 
        </div></div>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j14">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
              VM Arguments
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j15">
              <span data-feather="file" class="align-text-bottom"></span>
           Serialisation
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j16">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
           Java Applet Vs Applications
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j17">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
           Multithreading 
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j18">
              <span data-feather="file" class="align-text-bottom"></span>
           Executor Framework
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j19">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
            Design pattern in java
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j20">
              <span data-feather="file" class="align-text-bottom"></span>
           Compiler Vs Interpreter
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j21">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
           OOPS concept
            </a>
          </li>
          <li class="nav-item">
  <hr class="sidebar-divider">
  <div style="margin:auto">
            <div class="dropdown open">
                <p class="border-none outline-none "  id="triggerId"
                data-aria-expanded="true">
                <i class="fas fa-info-circle"></i>
                <span class="d-none d-sm-inline"><a class="dropdown-item" href="#j22">Access/Non-access modifier</a></span></p>
            <div class="dropdown-menu" aria-labelledby="triggerId">
                <a class="dropdown-item" href="#j23">visibilty scope</a>
                <a class="dropdown-item" href="#j24">Overriding rules</a>
            </div> 
        </div></div>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j25">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
            Enum
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j26">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
           Features of Java 8 to Java 19
            </a>
          </li>
          <li class="nav-item">
          <hr class="sidebar-divider">
          <div class="item">
             <a class="sub-btn" href="#j27"><i class="fas fa-info-circle"></i>Java 8<i class="fas fa-angle-right dropdown"></i></a>
            <div class="sub-menu">
                <a href="#j28" class="sub-item"> Functional Interface</a>
                <a href="#j29" class="sub-item">Lambda Expression</a>
                <a href="#j30" class="sub-item"> Stream APIs</a>
                <a href="#j31" class="sub-item">Method Reference</a>
                <a href="#j32" class="sub-item"> Map-Reduce APIs</a>
                <a href="#j33" class="sub-item"> Default Methods in interface</a>
                <a href="#j34" class="sub-item"> Switch to Java 8</a>
            </div>
            </div>
          </li>
         
          <li class="nav-item">
  <hr class="sidebar-divider">
  <div style="margin:auto">
            <div class="dropdown open">
                <p class="border-none outline-none "  id="triggerId"
                data-aria-expanded="true">
                <i class="fas fa-info-circle"></i>
                <span class="d-none d-sm-inline"><a class="dropdown-item" href="#j35">Different Java Edition</a></span></p>
            <div class="dropdown-menu" aria-labelledby="triggerId">
                <a class="dropdown-item" href="#j36">Java SE Varsion History</a>
                <a class="dropdown-item" href="#j37"> Java SE Vs Java EE </a>
                 <a class="dropdown-item" href="#ja"> Java Swing Vs Java FX </a>
            </div> 
        </div></div>
          </li>
          <li class="nav-item">
  <hr class="sidebar-divider">
  <div style="margin:auto">
            <div class="dropdown open">
                <p class="border-none outline-none "  id="triggerId"
                data-aria-expanded="true">
                <i class="fas fa-info-circle"></i>
                <span class="d-none d-sm-inline"><a class="dropdown-item" href="#j38">   Class loader in Java</a></span></p>
            <div class="dropdown-menu" aria-labelledby="triggerId">
                <a class="dropdown-item" href="#j39">Class loader working and types</a>
                <a class="dropdown-item" href="#j40">  Static/Dynamic Loading </a>
            </div> 
        </div></div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#j41">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
           Stackoverflow error in java
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j42">
              <span data-feather="file" class="align-text-bottom"></span>
            Static block initialiser
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j43">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
           Shallow copy/Deep copy 
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j44">
              <span data-feather="file" class="align-text-bottom"></span>
            Immutable Objects
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j45">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
           Garbage Collector 
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j46">
              <span data-feather="file" class="align-text-bottom"></span>
           JDBC/JPA/JSP
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#java">
              <span data-feather="file" class="align-text-bottom"></span>
              JSP/JPA/JDBC/Hibernate Usecase
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j47">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
           Java Card
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j48">
              <span data-feather="file" class="align-text-bottom"></span>
           Java applications in realtime
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j49">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
           Frameworks
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j50">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
              Diamond Warning<> [Java 7]
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j51">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
              Sealed class [JDK 15]
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#sealed">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
              Sealed class Example
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j52">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
              Annotation in Java
            </a>
          </li>
          <hr class="sidebar-divider">
          <li class="nav-item">
            <a class="nav-link" href="#j53">
              <span data-feather="shopping-cart" class="align-text-bottom"></span>
           Marker Interface
            </a>
          </li>
        </ul>

       
      </div>
      </div>
    </nav>
    </div></div>
    <div style="margin-top:15px;padding-top:10px;">
    <div id="google_element" style="float:right"></div>
  <script src="https://translate.google.com/translate_a/element.js?cb=loadGoogleTranslate"></script>
        <script>
           function loadGoogleTranslate(){
            new google.translate.TranslateElement("google_element");
           }
            </script>
    <label style="margin-top:35px;float:right" ><b><a href="<?=base_url().'Welcome/Blog'?>" ><button class="btn btn-success" >Back</button></a></b></label><br><hr>
<div class="content"  style="margin-top:5px" >
    <section id="j1" >
<h1>What is Java.</h1>
Java is a high-level, class-based, object-oriented programming language 
that is designed to have as few implementation dependencies.Java is a 
programming language and a platform. Java is a high level, robust, 
object-oriented and secure programming language. <br>
<b>Java main() Method – public static void main(String[] args)</b><br>
<b>1. Public </b><br>
It is an Access modifier, which specifies from where and who can 
access the method. Making the main() method public makes it globally 
available. It is made public so that JVM can invoke it from outside the 
class as it is not present in the current class.<br>
<b>2. Static</b><br>
It is a keyword that is when associated with a method, making it 
a class-related method. The main() method is static so that JVM can 
invoke it without instantiating the class. This also saves the 
unnecessary wastage of memory which would have been used by the 
object declared only for calling the main() method by the JVM.<br>
<b>3. Void </b><br>
It is a keyword and is used to specify that a method doesn’t 
return anything. As the main() method doesn’t return anything, 
its return type is void. As soon as the main() method terminates, 
the java program terminates too. Hence, it doesn’t make any 
sense to return from the main() method as JVM can’t do anything 
with the return value of it.<br>
<b>4. main </b><br>
It is the name of the Java main method. It is the identifier 
that the JVM looks for as the starting point of the java program. 
It’s not a keyword.<br>
<b>5. String[] args </b><br>
It stores Java command-line arguments and is an array of type 
java.lang.String class. Here, the name of the String array is 
args but it is not fixed and the user can use any name in place of it. <br>
<b>Why main() is static in Java?</b><br>
<ul>
  <li>Main() is static in java to make sure that it can be called without
    creating any instance.
  </li>

</ul>
    </section>
    <section id="j2">
<h1>Datatypes</h1><br>
<b>Primitive data types:</b> The primitive data types include boolean, char, byte, short, int, long, float and double.<br>
<b>Non-primitive data types:</b> The non-primitive data types include Classes, Interfaces, and Arrays.<br><br>
<table border="4" style="width:400px;height:400px;margin:auto">
  <tr>
    <th>Datatype</th><th>Default Values</th><th>Memory Used</th>
  </tr>
  <tbody>
  <tr>
    <td>boolean</td><td>false</td><td>1 bit</td>
  </tr>
  <tr>
    <td>char</td><td>'\u0000'</td><td>2 byte</td>
  </tr>
  <tr>
    <td>byte</td><td>0</td><td>1 byte</td>
  </tr>
  <tr>
    <td>short</td><td>0</td><td>2 byte</td>
  </tr>
  <tr>
    <td>int</td><td>0</td><td>4 byte</td>
  </tr>
  <tr>
    <td>long</td><td>0L</td><td>8 byte</td>
  </tr>
  <tr>
    <td>float</td><td>0.0f</td><td>4 byte</td>
  </tr>
  <tr>
    <td>double</td><td>0.0d</td><td>8 byte</td>
  </tr>
  </tbody>
</table>

    </section>
    <section id="j3">
<h1>Reserved Keywords</h1><br>
Java has 67 reserved words that have a predefined meaning in the language.<br>
You cannot use any of the reserved keywords as identifiers in your programs.<br>
The keywords const and goto are reserved, even though they are not currently 
used. true, false, and null might seem like keywords, but they are actually 
literals; you cannot use them as identifiers in your programs.
    </section>
    <section id="j4">
<h1>Exceptions/Errors</h1><br>
<b>Exceptions - </b>It is an event that occurs during the execution of the program 
and interrupts the normal flow of program instructions. These are the errors that 
occur at compile time and run time. It occurs in the code written by the developers. 
It can be recovered by using the try-catch block and throws keyword. <br>
There are two types of exceptions <br>
<ul>
  <li>checked exceptions</li>
  <li>unchecked exceptions</li>
</ul>
<br>
<b>Advantages of Exceptions</b><br>
<ul>
<li>It separates error handling code from regular code.</li>
<li>It has the ability to propagate error reporting up the call stack of methods.</li>
<li>The grouping or categorizing of exceptions is a natural outcome of the class hierarchy.</li>
</ul><br>
<b>Errors - </b>In Java, an error is a subclass of Throwable that tells that 
something serious problem is existing and a reasonable Java application should 
not try to catch that error. Generally, it has been noticed that most of the 
occurring errors are abnormal conditions and cannot be resolved by normal conditions. 
As these errors are abnormal conditions and should not occur, thus, error and 
its subclass are referred to as Unchecked Exceptions.<br>

    </section>
    <section id="j5">
<h1>Generics</h1><br>
Generics means parameterized types. The idea is to allow type (Integer, String, 
… etc., and user-defined types) to be a parameter to methods, classes, and interfaces. 
Using Generics, it is possible to create classes that work with different data types.<br>
The Object is the superclass of all other classes, and Object reference can refer 
to any object. These features lack type safety. Generics add that type of safety feature.<br>
Generics in Java are similar to templates in C++. For example, classes like HashSet, ArrayList, HashMap, etc., use generics very well.

    </section>
    <section id="j6">
<h1>Abstract classes vs Interface</h1><br>
<b>Abstract Class - </b>Abstract class can have abstract and non-abstract methods.<br>
Abstract class doesn't support multiple inheritance.<br>
Abstract class can have final, non-final, static and non-static variables<br>
Abstract class can provide the implementation of interface.<br>
The abstract keyword is used to declare abstract class.<br>
An abstract class can extend another Java class and implement multiple Java interfaces.<br>
An abstract class can be extended using keyword "extends".<br>
A Java abstract class can have class members like private, protected, etc.<br>
<br>
<b>Interface - </b>Interface can have only abstract methods. Since Java 8, it can have default and static methods also.<br>
Interface supports multiple inheritance.<br>
Interface has only static and final variables.<br>
Interface can't provide the implementation of abstract class.<br>
The interface keyword is used to declare interface.<br>
An interface can extend another Java interface only.<br>
An interface can be implemented using keyword "implements".<br>
Members of a Java interface are public by default.<br>

    </section>
    <section id="j7">
<h1>Super/this keyword</h1><br>
<b>“super” and “this” </b> Java are two predefined keywords, that cannot be used 
as an identifier.<br> “super” in Java is used to refer to methods, static and 
instance variables, constructors of an immediate parent class.<br> “this” in 
Java is used to refer to methods, static and instance variables, 
constructors of a current class.<br><br>
<b>super()</b> acts as immediate parent class constructor and should be first 
line in child class constructor.<br> <b>this()</b> acts as current class constructor 
and can be used in parametrized constructors. When invoking a superclass 
version of an overridden method the super keyword is used.<br>
<b>Can we use this () and super () in a method?</b><br>
No, we cannot use this() and super() in a method in java. <br>
<b>can a constructor call another constructor java?</b><br>
Constructor chaining refers to the ability to call a constructor inside another 
constructor. You can use a constructor chain either within the same class or even 
with another one. For the latter, the constructor should be through inheritance 
from the super class.The line inside a constructor that calls another constructor 
should be the first line of the constructor.<br>

    </section>
    <section id="j8">
<h1>Static/non-static members</h1>
<ul>
<li>A non-static class can have both static and non-static members .</li>
___________<br>
<li>Static classes are sealed and therefore cannot be inherited. 
  They cannot inherit from any class except Object.static methods are not inherited.<br>
If a superclass member is accessible by its simple name in the 
subclass (without the use of any extra syntax like super), that member is considered inherited.</li>
____________<br>
<li><b>Can a non static parent class have static child class. - No</b>
You are not able to call a non-static method of a parent class from a static method of a child class.
Your best option is to make the method non-static.</li>
_____________<br>
<li>Static class can't have non static members - Static classes can't be 
  instantiated in the first place, so even if you could declare non-static 
  (instance) members, they can never be accessed.</li>
_____________<br>
<li>The static nested class can't directly access outerField because it's 
  an instance variable of the enclosing class, OuterClass. The Java 
  compiler generates an error at the highlighted statement:<br>
public class OuterClass { <br>
String outerField = "Outer field";<br>
static class StaticNestedClass { <br>
void accessMembers(OuterClass outer) { <br>
// Compiler error: Cannot make a static reference to the non-static <br>
// field outerField System.out.println(outerField); }<br>
} } <br>

To fix this error, access outerField through an object reference:<br>

System.out.println(outer.outerField);</li>
</ul>
<b>In versions prior to java8 , we were able to execute our program without main() using static block but not from java 8.</b>
    </section>
    <section id="j9">
<h1>Superclass members</h1><br>
A class that is derived from another class is called a subclass 
(also a derived class, extended class, or child class). The class 
from which the subclass is derived is called a superclass (also a base 
class or a parent class).<br>
In other words the superclass, also known as the parent class , 
is the class from which a child class (or a subclass) inherits its constructors<br>
<b>The class named Object is the super class of every class in Java.</b>

    </section>
    <section id="j10">
<h1>JDK/JRE/JVM/JIT</h1><br>
<b>Java Virtual Machine (JVM)</b> is an abstract computing machine.<br>
<b>Java Runtime Environment (JRE)</b> is an implementation of the JVM.<br>
<b>Java Development Kit (JDK)</b> contains JRE along with various development tools 
like Java libraries, Java source compilers, Java debuggers, bundling and deployment tools.<br>
<b>Just In Time compiler (JIT)</b> is runs after the program has started executing, on the fly. 
It has access to runtime information and makes optimizations of the code for better performance.<br>
jdk is a software development environment that is used to develop,compile & execute java 
applications & applets.jre is implementation of jvm.jdk is a superset that includes jre , 
compiler & other system files & library.<br>

Main difference between JRE and JDK is that, you can not compile Java program using JRE. <br>
Tools required for compiling Java source file to create class files, i.e. javac, comes 
with JDK installation.JDK is something you will need to compile your code to make them 
executable by your JRE.You will need javac for compiling your code which is present in JDK.
Jre can only be use to run a java program and is developed as browser plugin. In order 
to run any Java program from browser.<br>
JRE was required to run applets like java app on users machines<br>

Firstly java is compiled to bytecode, which then either compiled, or interpreted depending on mood of JIT.<br>

<b>Similarity &  distinction  in JDK & jre -></b><br>
you downloaded just a JRE if you were only interested in 
running Java program's. If a programmer would like to 
execute a Java program using the Java command, they should install JRE.<br>
JDK includes all the Java tools, executables and binaries needed to 
run Java programs. This includes JRE, a compiler, a debugger, an 
archiver and other tools that are used in Java development.
    </section>
    <section id="j11">
<h1>Collections Framework</h1><br>
A <b>collection</b> is an object that represents a group of objects (such as the 
classic Vector class). A <b>collections framework</b> is a unified architecture 
for representing and manipulating collections, enabling collections to be 
manipulated independently of implementation details.<br>
<b>Benefits :</b><br>
Reducing the effort required to write the code by providing useful data structures and algorithms<br>
Java collections provide high-performance and high-quality data structures and algorithms thereby increasing the speed and quality<br>
Supports reusability of standard data structures and algorithms<br>
Inside collection framework , we have certain classes and interfaces.like-<br>
<b>1.The Collection Interface</b><br>
This enables you to work with groups of objects; it is at the top of the collections hierarchy.<br>
<b>2	The List Interface</b><br>
This extends Collection and an instance of List stores an ordered collection of elements.<br>
<b>3	The Set</b><br>
This extends Collection to handle sets, which must contain unique elements.<br>
<b>4	The SortedSet</b><br>
This extends Set to handle sorted sets.<br>
<b>5	The Map</b><br>
This maps unique keys to values.<br>
<b>6	The Map.Entry</b><br>
This describes an element (a key/value pair) in a map. This is an inner class of Map.<br>
<b>7	The SortedMap</b><br>
This extends Map so that the keys are maintained in an ascending order.<br>
<b>8	The Enumeration</b><br>
This is legacy interface defines the methods by which you can enumerate 
(obtain one at a time) the elements in a collection of objects. This legacy interface has been superceded by Iterator.<br>

    </section>
    <section id="j12">
<h1>Concurrent Collection</h1><br>
<b>Concurrent Collections</b> are thread-safe collection classes that we should 
use in a scenario where our code involves simultaneous access to a collection. 
Unlike collections, concurrent collections have a reliable behavior in a 
multi-threaded environment(with concurrent access to the collection).<br>
<b>ConcurrentHashMap - </b>provides a concurrent alternative to Hashtable or 
Synchronized Map classes with the aim to support a higher level of concurrency.<br>
Iterator of ConcurrentHashMap are fail-safe iterators that don't throw 
ConcurrencModificationException thus eliminates another requirement of locking 
during iteration which results in further scalability and performance.<br>
<b>CopyOnWriteArrayList and CopyOnWriteArraySet - </b>CopyOnWriteArrayList is a concurrent 
alternative of synchronized List. CopyOnWriteArrayList provides better concurrency than 
synchronized List by allowing multiple concurrent readers and replacing the whole list on write operation. <br>
write operation is costly on CopyOnWriteArrayList but it performs better when 
there are multiple readers, and the requirement of iteration is more than writing. 
Since CopyOnWriteArrayList Iterator also doesn't throw ConcurrencModificationException 
it eliminates the need to lock the collection during iteration. <br>
<b>BlockingQueue - </b>BlockingQueue makes it easy to implement producer-consumer design pattern 
by providing inbuilt blocking support for the put() and take() method. put() method 
will block if the Queue is full while the take() method will block if the Queue is empty. <br>
<b>Deque and BlockingDeque - </b>Deque interface is added in Java 6 and it extends 
Queue interface to support insertion and removal from both ends of Queue referred 
to as head and tail. Java6 also provides a concurrent implementation of Deque 
like ArrayDeque and LinkedBlockingDeque. <br>
    </section>
    <section id="j13">
<h1>Traditional Collection</h1>
The Java collections framework provides a set of interfaces and classes to implement various data structures and algorithms.
Use of collections framework classes and interfaces is a biggest merit of using Java .
<br><br>

Different classifications under traditional collections - <br><br>
<img src="https://cdn.programiz.com/sites/tutorial2program/files/Java-Collections.png" width="100%" height="400px"/>
<br><br>
<b>Collections Framework Vs. Collection Interface</b><br>
The Collection interface is the root interface of the collections framework. 
The framework includes other interfaces as well: Map and Iterator.<br>
Here are the subinterfaces of the Collection Interface:<br><br>

<b>List Interface</b><br>
The List interface is an ordered collection that allows us to add and remove elements like an array.</b>

<b>Set Interface</b><br>
The Set interface allows us to store elements in different sets similar to the set in mathematics. It cannot have duplicate elements.</b>
<b>Queue Interface</b><br>
The Queue interface is used when we want to store and access elements in First In, First Out manner.<br>
<b>Java Map Interface</b><br>
In Java, the Map interface allows elements to be stored in key/value pairs. Keys are unique names that can be used to access a 
particular element in a map. And, each key has a single value associated with it.<br>

<b>Java Iterator Interface</b><br>
In Java, the Iterator interface provides methods that can be used to access elements of collections.<br>
    </section>
    <section id="j14">
<h1> VM Arguments</h1>
The values that we passed I side vm arguments  r to be accessed and 
taken care by the JVM. Present inside system properties. We can 
access vm argument file using System.getProperty() method.<br>
What data to pass inside VM Argument : <br>
<ul>
<li>* Any info that needs to be available  in the system  properties.</li>
<li>* Controlling the Heap size , data memory size , code cache size which is the tuning of the JVM .</li>
<li>* To give any algorithm  for garbage collection. </li>
<li>* To run any jar file from cmd using vm arguments. </li>
</ul><br>
VM arguments  data is processed by java SW interpreter
.eg... When we run some commands like  java -Dxyz p1 p2 
Here, java -D is interpreter which makes data xyz available  to the JVM. P1 & P2 r program  argument <br>
<b>Program Arguments  Vs VM Arguments: </b><br>
<ul>
<li>The values passed inside the program  arguments  r actually 
the command line parameters  that r accepted by the main(String[] args) Arguments .</li<br>
<li>VM arguments  r of knowledge  to the JVM.</li<br>
</ul>
___________________________<br>
<b>Setting heap size using vm arguments :</b><br>
-Xms128mere : minimum heap size<br>
-Xmx1024mere : maximum  heap size<br>
    </section>
    <section id="j15">
<h1> Serialisation</h1><br>
<b>Serialization -</b> a mechanism of writing the state of an object into a byte-stream.<br>
<b>Deserialisation -</b> byte-stream is converted into an object. The serialization and 
deserialization process is platform-independent, it means you can serialize an object 
on one platform and deserialize it on a different platform.<br>
For serializing the object, we call the <b>writeObject()</b> method of <b>ObjectOutputStream class</b>,
 and for deserialization we call the <b>readObject()</b> method of <b>ObjectInputStream class</b>.<br>
    </section>
    <section id="j16">
<h1>Java Applet Vs Applications</h1><br>
Applet id a most popular web application language that uses java for 
websites development. <br>
Applet is a program that runs on web browser .ie..it always require internet connection for functioning. 
Which is not so incase of a java application. Java applications are stand alone applications that 
needs a JRE as an environment to execute the application. Applets program have a specific life cycle to 
execute it's code & do not necessarily require main() to start its execution and since applet require 
browser based access than it is also more prone to security threats.Hence, an applet program needs more security.<br> 
For any java program that runs on internet uses applet in it.It can perform arithmetic operation, 
display some graphics, play sounds,accepting of user inputs,creating animations.ie..for any interactive 
multimedia files animation etc.we can use java as an applet.It can represent a interactive web documents 
that can contain not only text but also other media files ,movies,animation,sounds,graphics etc.in it's  web page. 
    </section>
    <section id="j17">
<h1>  Multithreading </h1>
<b>Can a given thread be started  & called twice ?</b><br>
No, we cannot start Thread again, doing so will 
throw runtimeException java.lang.IllegalThreadStateException
a Thread can only be started once.If a Thread needs to be 
run more than once, then one should make an new instance 
of the Thread and call start on it. <b>(When a thread is started it called start() which internally called run() ).</b>
So , After starting a thread, it can never be started again. Hence, we can't  call a same thread multiple times.<br>
<b>Problem with multithreading</b><br>

Increased difficulty level of code.<br>
Deadlock occurrence if not using synchronize<br>
Overhead due to thread switching <br>
    </section>
    <section id="j18">
<h1>Executor Framework</h1>
<b>Executor Framework in java -></b>A framework having a bunch of components that are used for managing worker 
threads efficiently is referred to as Executor Framework. 
It is used to run the Runnable objects without creating new 
threads every time and mostly re-using the already created 
threads. It is helpful  in resolving IllegalThreadStateException.<br>
In order to use the executor framework, we have to create a thread pool for executing the task by submitting that task to that thread pool.<br>

<b>Executor Importance over Thread class </b><br>
We need to create a large number of threads for adding a new thread without any 
throttling for each and every process. Due to which it requires more memory and 
cause wastage of resource. When each thread is swapped, the CPU starts to spend 
too much time.<br>
When we create a new thread for executing a new task cause overhead of thread 
creation. In order to manage this thread life-cycle, the execution time 
increase respectively.<br>

    </section>
    <section id="j19">
<h1>  Design pattern in java</h1>
Design Pattern is a description or guideline to solve a problem 
that occurs repeatedly while developing a software Applications. 
Design Patterns will help us in fixing performance and memory related issues.<br>
With strong design pattern you will complete your development 
earlier than the expected time and number of bugs will also be very negligible.<br>
Design Patterns help in finding the solution of a complex problem.<br>
Using design patterns, we can make our code loosely-coupled.<br>
Furthermore, it will have the option of reusable codes, which 
reduce the total development cost of the application.<br>
<ul>
<li>* creational design pattern ( single ton,prototype etc)</li>
<li>* structural design pattern</li>
<li>* behavioral design pattern</li>
</ul>
    </section>
    <section id="j20">
<h1>  Compiler Vs Interpreter</h1><br>
When we compile our code than code produces an executable file but 
incase of interpreter no executable file is produced as the code 
is executed line by line that9 why it is slower and also debugging 
through interpreter is easy.<br>
The code of interpreter is portable ie.. It can run on any machine where interpreter is installed.<br>
__________<br>
<b>Hybrid approach -</b> it combines the functioning of both compiler&  interpreter.
The compiler generates byte code from our code and than that byte code can 
be easily executed  in any machine having interpreter installed in it.<br>
____________<br>
<ul>
<li><b>Compiled language -</b> c,c++</li>
<li><b>Interpreted lang -</b> js, php </li>
<li><b>Hybrid lang -</b> java, c#, kotlin </li>
</ul>
    </section>
    <section id="j21">
<h1>OOPS concept</h1><br>
OOPs refers to languages that use objects in programming, they use 
objects as a primary source to implement what is to happen in the code.
Objects are seen by the viewer or user, performing tasks assigned by you. <br>
Pillars of OOPs -<br>
Abstraction<br>
Encapsulation<br>
Inheritance<br>
Polymorphism<br>
<ul>
<li>Compile-time polymorphism</li>
<li>Runtime polymorphism</li<br>
</ul>

    </section>
    <section id="j22">
<h1> Access/Non-access modifiers</h1><br>
<b>Non-access modifier :</b>
The <b>static modifier</b> for creating class methods and variables.<br>
The <b>final modifier</b> for finalizing the implementations of classes, methods, and variables.<br>
The <b>abstract modifier</b> for creating abstract classes and methods.<br>
The <b>synchronized and volatile modifiers</b>, which are used for threads.<br>
<b>Access Modifier :</b>
<b>Private:</b> The access level of a private modifier is only within the class. 
It cannot be accessed from outside the class.<br>
<b>Default:</b> The access level of a default modifier is only within the package. 
It cannot be accessed from outside the package. If you do not specify any access level, it will be the default.<br>
<b>Protected:</b> The access level of a protected modifier is within the package and outside 
the package through child class. If you do not make the child class, it cannot be accessed from outside the package.<br>
<b>Public:</b> The access level of a public modifier is everywhere. It can be accessed from 
within the class, outside the class, within the package and outside the package.<br>
<br>
Modifiers public, static, strictFp etc 
they all can be placed in any order before giving the 
return type.<br><b>eg... public static int sum() same as static public  int sum().</b>
<br>
<b>Access modifiers visibility sequence is : private < default < protected < public 
which means private is the most restrictive & public is the least restrictive modifier.</b>
    </section>
    <section id="j23">
<h1>visibilty scope</h1><br>
<b>Private: </b> can access the private modifier only within the same class and not from outside the class.<br>
<b>Default:</b> We can access the default modifier only within the same package and not from outside the 
package. And also, if we do not specify any access modifier it will automatically consider it as default.<br>
<b>Protected:</b> We can access the protected modifier within the same package and also from outside the 
package with the help of the child class. If we do not make the child class, we cannot access it from 
outside the package. So inheritance is a must for accessing it from outside the package.<br>
<b>Public:</b> We can access the public modifier from anywhere. We can access public modifiers from within 
the class as well as from outside the class and also within the package and outside the package<br>

    </section>
    <section id="j24">
<h1>Overriding rules</h1><br>
The following rules for inherited methods are enforced for overridding class-<br>

Methods declared <b>public</b> in a superclass also must be public in all subclasses.<br>
Methods declared <b>protected</b> in a superclass must either be protected or public in subclasses; they cannot be private.<br>
Methods declared <b>private</b> are not inherited at all, so there is no rule for them.<br>
<br>
if parent is protected then child as public is allowed.
Child class must not throw newer or broader checked exceptions. . May throw 
narrower(below in class hierarchy) checked exceptions or any unchecked exception.
    </section>
    <section id="j25">
<h1>  Enum</h1><br>
Enum is a type like a class or interface but it can't extend any other class.
We cannot create enum instance outside of enum b'coz it can't have public 
constructor in Enum and compiler doesn't allow any public constructor inside the 
Enum. Enum can only allow private constructor .
An enum type is a special data type that enables for a variable to be a set 
of predefined constants. The variable must be equal to one of the values 
that have been predefined for it.<br>
eg ... <br>
enum Level {<br>
  LOW,<br>
  MEDIUM,<br>
  HIGH<br>
}<br>
You can access enum constants with the dot syntax:<br>
Level myVar = Level.MEDIUM; //output - MEDIUM<br>

    </section>
    <section id="j26">
<h1> Features of Java 8 to Java 19</h1>
<b>Java8 -></b> Lamdas, stream APIs
<b>Java9 -></b> Interface included  private methods. 
 Introduced  use of advance methods [helper methods]  
 under collections interface.<br>eg...
List<String> list = List.of("one", "two", "three");<br>
Set<String> set = Set.of("one", "two", "three"); <br>
Map<String, String> map = Map.of("foo", "one");<br>
____________<br>
<b>Switch  from java8 to java9 -></b><br>
Build tools (Maven, Gradle etc.) and some libraries initially
had bugs with versions Java versions > 8 and needed updated.<br.>
Hence,  came up with java9.
Up until Java 8 you were pretty much using Oracle’s JDK builds 
and you did not have to care about licensing. Oracle changed 
the licensing scheme In 2019.<br>
Up until Java 8, the Oracle website offered JREs and JDKs as 
separate downloads - even though the JDK also always included 
a JRE in a separate folder. With Java 9 that distinction was 
basically gone, and you are always downloading a JDK. 
The directory structure of JDKs also changed, with not having 
an explicit JRE folder anymore.<br>
<b>Naming scheme  before  java9 --</b> Java 8 can also be called 1.8, Java 5 can be called 1.5 etc. 
<br>
________________<br>
<b>Starting from Java 10 -> </b>we can run Java source files without having 
to compile them first. A step towards scripting.<br>
_____________<br>
<b>Java10 -></b> Unique garbage collection mechanism,
Introduction of the "var"-keyword, also called local-variable type inference.<br>eg...
String myName = "Marco"; //pre java10<br>
var myName = "Marco";  // with java10<br>
<b>Java14 -></b> Got standardized  switch case structure  than java 12,13.Enhanced switch  case structure  would  look like -<br>
int numLetters = switch (day) { <br>
case MONDAY, FRIDAY, SUNDAY -> 6;<br>
 case TUESDAY -> 7; <br>
default -> { String s = day.toString(); <br>
int result = s.length(); <br>
yield result; }<br>
 };<br>
<b>Java16 -></b> Java 16 (no need of casting)<br>
Pattern Matching for instanceof<br>
Instead of:<br>
if (obj instanceof String) { String s = (String) obj; // e.g. s.substring(1) }<br>
You can now do this:<br>
if (obj instanceof String s) { // Let pattern matching do the work! // ... s.substring(1) }<br>
    </section>
    <section id="j27">
<h1>Java 8</h1><br>
It includes a huge upgrade to the Java programming model and a coordinated evolution of the JVM, Java language, and libraries.<br>
he fact that Java 8 is an LTS (Long-Term Support) version is one of the main reasons for its continued popularity.
It includes all the new features such as -
<b>Lambdas</b><br>
<b> Streams</b><br>
<b> Optionals</b><br>
<b> Functional Interfaces</b><br>
<b> Parallel Programming</b> <br>
Only Java 8 (2014) and Java 11 (2018) have been recognised as having LTS since the policy was implemented.<br>

    </section>
    <section id="j28">
<h1> Functional Interface</h1>
<b>Predefined functional interface benefit -></b><br>
make our programming easier. Moreover, Predefined 
Functional Interfaces include most commonly used 
methods which are available to a programmer by default. <br>
They will obviously save our development time and 
minimize chances of mistakes.
    </section>
    <section id="j29">
<h1>Lambda Expression</h1><br>
A lambda expression is a short block of code which takes in parameters 
and returns a value. Lambda expressions are similar to methods, but 
they do not need a name and they can be implemented right in the body of a method.<br>
The simplest lambda expression contains a single parameter and an expression:<br>
parameter -> expression<br>
To use more than one parameter, wrap them in parentheses:<br>
(parameter1, parameter2) -> expression<br>
Lambda expressions can only be used in context of Functional interfaces. You 
can assign a lambda expression to a single abstract method (i.e. To a Functional interface). 
If you could assign a lambda expression to an interface containing more than one abstract 
method (i.e. a non functional interface), the lambda expression could only have 
implemented one of its methods, leaving the other methods unimplemented. That’s 
why Lambda expressions don’t have method name, return types etc. as it talks about a single abstract method.<br>
Lambda expressions are a new and important feature included in Java SE 8. They provide a clear 
and concise way to represent one method interface using an expression. Lambda expressions also 
improve the Collection libraries making it easier to iterate through, filter, and extract data from a Collection .<br>
<b>Lambda Advantages</b><br>
Lambda enables you to use functions with pre-trained machine learning (ML) models to inject 
artiﬁcial intelligence into applications more easily. A single application programming interface (API) 
request can classify images, analyze videos, convert speech to text, perform natural language processing, and more.<br>
<b>Lambda Disadvantage</b><br>
they are so syntactically simple to write but so tough to understand if you're not used to them. So if you 
have to quickly determine what the code is doing, the abstraction brought in by lambdas, even as it simplifies 
the Java syntax, will be hard to understand quickly and easily.<br>

    </section>
    <section id="j30">
<h1>Stream APIs</h1><br>
Stream API is used to process collections of objects. A stream is a sequence of objects that 
supports various methods which can be pipelined to produce the desired result.<br>
Stream API contains various inbuilt methods that supports aggregate methods.It follows 
a pipeline type of structure starting from source than It may have no or some 
intermediate operations followed by terminal operations. intermediate operations r optional.<br>
it is a technique to make processing of collection’s data easy by supporting functional-style operations.<br>

Sequential stream :<br>
Parallel stream : It allows us to do operations on collection in thread safe mode.<br>
The features of Java stream are –<br>

A stream is not a data structure instead it takes input from the Collections, Arrays or I/O channels.<br>
Streams don’t change the original data structure, they only provide the result as per the pipelined methods.<br>
Each intermediate operation is lazily executed and returns a stream as a result, hence various intermediate 
operations can be pipelined. Terminal operations mark the end of the stream and return the result.<br>
Different Operations On Streams-<br>
<b>Intermediate Operations:</b>(),filter,sorted()<br>
<b>Terminal Operations:</b>(),foreach(),reduce()<br>


    </section>
    <section id="j31">
<h1>Method Reference</h1><br>
Method reference is used to refer method of functional interface. It is compact and easy form of 
lambda expression. Each time when you are using lambda expression to just referring a method, 
you can replace your lambda expression with method reference.<br>
It is a shorthand notation of a lambda expression to call a method. We can replace lambda 
expression with method reference (:: operator) to separate the class or object from the method name.<br>
Sometimes, a lambda expression only calls an existing method. In those cases, it looks clear to refer to 
the existing method by name. The method references can do this, they are compact, easy-to-read as 
compared to lambda expressions. A method reference is the shorthand syntax for a lambda expression 
that contains just one method call. <br>
<b>To print all elements in a list</b><br>
<b>Using Lambda - </b>list.forEach(s -> System.out.println(s)); <br>
<b>Using Method Reference - </b>list.forEach(System.out::println); <br>

    </section>
    <section id="j32">
<h1>Map-Reduce APIs</h1><br>
It is used to implement MapReduce type operations. Essentially we map a set of values then we reduce 
it with a function such as average or sum into a single number.<br>
The MapReduce is a paradigm which has two phases, the mapper phase, and the reducer phase. 
In the Mapper, the input is given in the form of a key-value pair. The output of the Mapper 
is fed to the reducer as input. The reducer runs only after the Mapper is over. The reducer 
too takes input in key-value format, and the output of reducer is the final output.<br>
<b>Usage of MapReduce</b><br>
It can be used in various application like document clustering, distributed sorting, and web link-graph reversal.<br>
It can be used for distributed pattern-based searching.<br>
We can also use MapReduce in machine learning.<br>
It was used by Google to regenerate Google's index of the World Wide Web.<br>

    </section>
    <section id="j33">
<h1> Default Methods in interface</h1>
<b>Default  methods in interface (java8) :</b>
declare a method as default which has very less or 
negligible chances to be overridden by subclasses. <br>
The primary idea of including default method in 
interface is that don’t force the implementing classes 
to override it.<br> Till JDK 1.7, all implementing classes 
were supposed to override the method declared in the 
interface and provide the concrete implementation of 
the methods. But after the introduction of default method 
in Interface, implementing classes are free to either 
override it or not as per the required behavior.<br>
default method save effort in writing extra lines of code.
    </section>
    <section id="j34">
<h1> Switch to Java 8</h1><br>
Lambda expression helps us to write our code in functional style. 
It provides a clear and concise way to implement SAM interface(Single Abstract Method)
 by using an expression. It is very useful in collection library in which it 
 helps to iterate, filter and extract data.<br>
 Java 8 is useful due to the use of certain shorthand method/class properties like-<br>
<b>Lambda Expression</b><br>
Lambda expression helps us to write our code in functional style. It provides a clear 
and concise way to implement SAM interface(Single Abstract Method) by using an expression. 
It is very useful in collection library in which it helps to iterate, filter and extract data.<br>
 <b>Optional</b><br>
 Java introduced a new class Optional in Java 8. It is a public final class 
 which is used to deal with NullPointerException in Java application. We must 
 import java.util package to use this class. It provides methods to check the 
 presence of value for particular variable.<br>
 <b>Functional Interface</b><br>
 An Interface that contains only one abstract method is known as functional interface. 
 It can have any number of default and static methods. It can also declare methods of object class.
Functional interfaces are also known as Single Abstract Method Interfaces (SAM Interfaces).<br>
<b>Method Reference</b><br>
Java 8 Method reference is used to refer method of functional interface . It is compact 
and easy form of lambda expression. Each time when you are using lambda expression to just 
referring a method, you can replace your lambda expression with method reference.<br>
<b>forEach()</b><br>
Java provides a new method forEach() to iterate the elements. It is defined in Iterable and Stream interfaces.
It is a default method defined in the Iterable interface. Collection classes which extends 
Iterable interface can use forEach() method to iterate elements.<br>
<b>DateTime API</b><br>
Java has introduced a new Date and Time API since Java 8. The java.time package contains Java 8 Date and Time classes.<br>
<b>Default Methods</b><br>
Java provides a facility to create default methods inside the interface. Methods which are defined inside the interface and 
tagged with default keyword are known as default methods. These methods are non-abstract methods and can have method body.<br>


    </section>
    <section id="j35">
<h1>Different Java Edition</h1>
Java is distributed in three different editions: Java Standard Edition (Java SE), 
Java Enterprise Edition (Java EE) and Java Micro Edition (Java ME).
A new introduced is JavaFX.<br>
<b>JavaSE = Standard Edition.</b> This is the core Java programming platform. 
It contains all of the libraries and APIs that any Java programmer should 
learn (java.lang, java.io, java.math, java.net, java.util, etc...).<br>
<b>Java EE = Enterprise Edition.</b>
The Java platform (Enterprise Edition) differs from the Java Standard Edition 
Platform (Java SE) in that it adds libraries which provide functionality to 
deploy fault-tolerant, distributed, multi-tier Java software, based largely 
on modular components running on an application server.<br>
In other words, if your application demands a very large scale,
distributed system, then you should consider using Java EE. Built on top of 
Java SE, it provides libraries for database access (JDBC, JPA), remote method 
invocation (RMI), messaging (JMS), web services, XML processing, and defines 
standard APIs for Enterprise JavaBeans, servlets, portlets, Java Server Pages, etc...<br>

<b>Java ME = Micro Edition.</b> This is the platform for developing applications for 
mobile devices and embedded systems such as set-top boxes. Java ME provides a subset 
of the functionality of Java SE, but also introduces libraries specific to mobile devices. 
Because Java ME is based on an earlier version of Java SE, some of the new language 
features introduced in Java 1.5 (e.g. generics) are not available.<br>
<b>Java FX -</b> JavaFX is a software platform for creating and delivering desktop applications, 
as well as rich web applications that can run across a wide variety of devices. 
JavaFX has support for desktop computers and web browsers on Microsoft Windows, Linux, and macOS, 
as well as mobile devices running iOS and AndroidJavaFX is a set of graphics and media packages 
that enables developers to design, create, test, debug, and deploy rich client applications 
that operate consistently across diverse platforms.<br>

If you are new to Java, definitely start with Java SE.
    </section>
    <section id="j36">
<h1>Java SE Version History</h1>
 <table border="2" >
  <tr>
    <th>Java SE Version</th><th>Version Number</th><th>
Release Date</th>
  </tr>
  <tr>
    <td>JDK 1.0(Oak)</td><td>1.0</td><td>January 1996</td>
  </tr>
  <tr>
    <td>JDK 1.1</td><td>1.1</td><td>February 1997</td>
  </tr>
  <tr>
    <td>J2SE 1.2</td><td>1.2</td><td>December 1998</td>
  </tr>
  <tr>
    <td>J2SE 1.3</td><td>1.3</td><td>May 2000</td>
  </tr>
  <tr>
    <td>J2SE 1.4(Merlin)</td><td>1.4</td><td>February 2002</td>
  </tr>
  <tr>
    <td>J2SE 5.0(Tiger)</td><td>1.5</td><td>September 2004</td>
  </tr>
  <tr>
    <td>Java SE 6(Mustang)</td><td>1.6</td><td>December 2006</td>
  </tr>
  <tr>
    <td>Java SE 7(Dolphin)</td><td>1.7</td><td>July 2011</td>
  </tr>
  <tr>
    <td>Java SE 8</td><td>1.8</td><td>March 2014</td>
  </tr>
  <tr>
    <td>Java SE 9</td><td>9</td><td>September, 21st 2017</td>
  </tr>
  <tr>
    <td>Java SE 10</td><td>10</td><td>March, 20th 2018</td>
  </tr>
  <tr>
    <td>Java SE 11</td><td>11</td><td>September, 25th 2018</td>
  </tr>
  <tr>
    <td>Java SE 12</td><td>12</td><td>March, 19th 2019</td>
  </tr>
  <tr>
    <td>Java SE 13</td><td>13</td><td>September, 17th 2019</td>
  </tr>
  <tr>
    <td>Java SE 14</td><td>14</td><td>March, 17th 2020</td>
  </tr>
  <tr>
    <td>Java SE 15</td><td>15</td><td>September, 15th 2020</td>
  </tr>
  <tr>
    <td>Java SE 16</td><td>16</td><td>March, 16th 2021</td>
  </tr>
  <tr>
    <td>Java SE 17</td><td>17</td><td>September, 14th 2021</td>
  </tr>
  <tr>
    <td>Java SE 18</td><td>18</td><td>March, 22nd 2022</td>
  </tr>
  <tr>
    <td>Java SE 19</td><td>19</td><td>September, 20th 2022</td>
  </tr>
  <tr>
    <td>Java SE 20</td><td>20</td><td>March, 21st 2023</td>
  </tr>
 </table>
    </section>
    <section id="j37">
<h1>Java SE Vs Java EE</h1>
Java is synonymous with Java Standard Edition (Java SE) or Core Java.
 Java EE, on the other hand, provides APIs and is typically used to run larger applications.<br>
<b>JavaSE -</b> It defines everything from the basic types and objects of the Java programming 
language to high-level classes that are used for networking, security, database access, graphical 
user interface (GUI) development, and XML parsing.<br>

In addition to the core API, the Java SE platform consists of a virtual machine, development tools, 
deployment technologies, and other class libraries and toolkits commonly used in Java technology applications.<br>
<b>Java EE</b>
The Java EE platform is built on top of the Java SE platform. The Java EE platform provides an API and 
runtime environment for developing and running large-scale, multi-tiered, scalable, reliable, and secure network applications.<br>
    </section>
    <section id="ja">
<h1>Java Swing Vs Java FX</h1><br>
Java Swing helps programmers create graphical user interface (GUI) applications, and helps make them cleaner and more efficient.<br>
With Swing, developers are free to create labels, buttons, and other UI components.<br>
Java FX is the newly introduction in Java edition history and came after Swing.
FX behaves as a GUI library and lends itself to efficient and rapid development of desktop apps. Java FX has a modern design 
and provides developers with easy access to Rich Internet Application.It gives added support
to Swing like - to easily add to create blurs, shadows, and other textural touch-ups.<br>
<b>Key differences :</b><br><br>
Swing sees lots of use in GUI creation,and Desktop applications come together more easily when we use FX.<br>
Swing has a wider range of UI components compared to FX, but FX adds more all the time.Swing can 
provide UI components with a decent look and feel, whereas JavaFX can provide rich internet application having a modern UI.<br>
Swing has a more sophisticated set of GUI components, whereas JavaFX has a decent number of UI .<br>
components available but lesser than what Swing provides.<br>
 FX offers consistent support for MVC, while Swing’s MVC support is not equal across all platforms.<br>
 FX adds modern touches to your project and makes adding animation and special effects a snap. FX is also the 
 choice for folks who develop mobile apps—it is much more geared to work with mobile programs than Swing.<br>

    </section>
    <section id="j38">
<h1>Class loader in Java</h1><br>
The Java Class Loader is a part of the Java Runtime Environment that dynamically 
loads Java classes into the Java Virtual Machine. Usually classes are only loaded 
on demand. The Java run time system does not need to know about files and file 
systems as this is delegated to the class loader.<br>
There are three types of built-in ClassLoader in Java.<br>
<b>Bootstrap Class Loader – </b> It loads JDK internal classes. It loads rt.jar and other core classes for example java.lang.* package classes.<br>
<b>Extensions Class Loader – </b> It loads classes from the JDK extensions directory, usually $JAVA_HOME/lib/ext directory.<br>
<b>System Class Loader – </b> classloader loads classes from the current classpath.<br>

    </section>
    <section id="j39">
<h1>Class loader working and types</h1><br>
Every class loader has a predefined location, from where they load class files. 
The <b>bootstrap class loader</b> is responsible for loading standard JDK class files f
rom rt.jar and it is the parent of all class loaders in Java. 
The <b>bootstrap class loader</b> doesn't have any parents if you call 
String.class.getClassLoader() it will return null and any code based on that may 
throw NullPointerException in Java. The bootstrap class loader is also known as Primordial ClassLoader in Java.  <br>
<b>Extension ClassLoader</b> delegates class loading request to its parent, Bootstrap, and if unsuccessful, 
loads class form jre/lib/ext directory or any other directory pointed by java.ext.dirs system property. 
Extension ClassLoader in JVM is implemented by sun.misc.Launcher$ExtClassLoader. <br>

The third <b>default class loader</b> used by JVM to load Java classes is called System or Application class 
loader and it is responsible for loading application-specific classes from CLASSPATH environment variable, 
-classpath or -cp command line option, Class-Path attribute of Manifest file inside JAR. <br>

<b>Application class loader </b> a child of Extension ClassLoader and its implemented by 
sun.misc.Launcher$AppClassLoader class. Also, except for the Bootstrap class loader, which is 
implemented in the native language mostly in C,  all  Java class loaders are implemented using java.lang.ClassLoader.<br>

    </section>
    <section id="j40">
<h1>Static/Dynamic Loading</h1><br>
Static loading refers to loading the whole program into the main memory before executing the 
program. Dynamic loading refers to the process of loading a program into the main memory on demand.<br>
In Java <b>static binding </b> to the execution of a program where type of object is 
determined/known at <b>compile time</b> i.e when compiler executes the code it know the type of 
object or class to which object belongs. While in case of <b>dynamic binding</b> the type of 
object is determined at <b>runtime</b>.

    </section>
    <section id="j41">
<h1>Stackoverflow error in java</h1><br>
StackOverflowError is a runtime error which points to serious problems that cannot be 
caught by an application. The java. lang. StackOverflowError indicates that the application 
stack is exhausted and is usually caused by deep or infinite recursion.<br>
The most-common cause of stack overflow is excessively deep or infinite recursion, in which a function calls itself 
so many times that the space needed to store the variables and information 
associated with each call is more than can fit on the stack.

<b>How do I fix stackoverflow error in java?</b><br>
Carefully inspecting the error stack trace and looking for the repeating pattern of 
line numbers enables locating the line of code with the recursive calls. When the 
line is identified, the code should be examined and fixed by specifying a proper 
terminating condition.<br>

    </section>
    <section id="j42">
<h1> Static block initialiser</h1><br>
a static block executes code before the object initialization. A static block is a block of code with a static keyword.
A static initialization block is mostly used for changing the default value 
of static variables. A static variable in Java is a variable that belongs 
to the class and is initialized during the loading of the class into memory. 
Static variables are initialized only once, at the start of the execution.<br>
Instance variables are initialized using initialization blocks. However, 
the static initialization blocks can only initialize the static instance variables. 
These blocks are only executed once when the class is loaded. There can be multiple 
static initialization blocks in a class that is called in the order they appear in the program.<br>

    </section>
    <section id="j43">
<h1> Shallow copy/Deep copy </h1><br>
In shallow copy, only fields of the primitive data type are copied while the 
objects' references are not copied. Deep copy involves the copy of primitive 
data types as well as to object references. There is no hard and fast rule 
as to when to do shallow copy and when to do a deep copy.<br>
A shallow copy constructs a new compound object and then (to the extent possible) 
inserts references into it to the objects found in the original. A deep copy 
constructs a new compound object and then, recursively, inserts copies into 
it of the objects found in the original.<br>
<b>In shallow copy, the created reference will point to the original object while 
in deep copy , the newly created object will be independent of the original 
object.ie.. the reference will point to the memory location of the newly 
created object not the original object.</b><br>

    </section>
    <section id="j44">
<h1> Immutable Objects</h1><br>
An object is considered immutable if its state cannot change after it is 
constructed. Maximum reliance on immutable objects is widely accepted as 
a sound strategy for creating simple, reliable code. Immutable objects 
are particularly useful in concurrent applications.<br>
its state doesn't change after it has been initialized. 
For example, String is an immutable class and, once instantiated, the value of a String object never changes<br>
Immutable object remains in 1 state in which they r created throughout as they 
don't allow any change with them Hence, they r thread safe.<br>
The internal state of program is consistent even if there r some exception.

    </section>
    <section id="j45">
<h1> Garbage Collector </h1><br>
Garbage collection in Java is the automated process of deleting code 
that's no longer needed or used. This automatically frees up memory space and 
ideally makes coding Java apps easier for developers. Java applications are 
compiled into bytecode that may be executed by a JVM.<br>
<b>Advantage of Garbage Collection</b><br>
It makes java memory efficient because garbage collector removes the unreferenced objects from heap memory.<br>
It is automatically done by the garbage collector(a part of JVM) so we don't need to make extra efforts.<br>
<b>finalize() method</b>
The finalize() method is invoked each time before the object is garbage collected. This method can be used to perform cleanup processing. <br>
<b>gc() method</b>
The gc() method is used to invoke the garbage collector to perform cleanup processing. The gc() is found in System and Runtime classes.<br>

gc()(static method) is use to call garbage collector to perform clean up processing present 
in system & runtime class to unreference & destroy the object than garbage collector thread 
calls finalize method(protected) present in object class . It is a method defined in Object class. <br>
Garbage collector calls this method to perform clean up activities before destroying the object from the memory.
    </section>
    <section id="j46">
<h1> JDBC/JPA/JSP/Hibernate</h1><br>
JDBC is a programming-level interface for Java applications that communicate with a database. An application uses this API to communicate 
with a JDBC manager. It's the common API that our application code uses to communicate with the database.<br>
JPA is a Java standard that allows us to bind Java objects to records in a relational database. It's one possible approach to Object 
Relationship Mapping(ORM), allowing the developer to retrieve, store, update, and delete data in a relational database using Java objects. <br>
<b>JPA vs JDBC</b><br>
JDBC allows us to write SQL commands to read data from and update data to a relational database. JPA, unlike JDBC, allows developers to 
construct database-driven Java programs utilizing object-oriented semantics.<br>
When associating database tables in a query with JDBC, we need to write out the full SQL query, while with JPA, we simply 
use annotations to create one-to-one, one-to-many, many-to-one, and many-to-many associations.<br>
JDBC is database-dependent, which means that different scripts must be written for different databases. On the other side, 
JPA is database-agnostic, meaning that the same code can be used in a variety of databases with few (or no) modifications.<br>
<br>
<b>Pros and Cons</b><br>
The most obvious benefit of JDBC over JPA is that it's simpler to understand. On the other side, 
if a developer doesn't grasp the internal workings of the JPA framework or database design, 
they will be unable to write good code.<br>


</section>
    <section id="java">
<h1>JSP/JPA/JDBC/Hibernate Usecase</h1><br>
JPA is thought to be better suited for more sophisticated applications by many 
developers. But, JDBC is considered the preferable alternative if an application 
will use a simple database and we don't plan to migrate it to a different database vendor.<br>
a well-tested and robust framework is handling the interaction between the database and the Java 
application, we should see a decrease in errors from the database mapping layer when using JPA.<br>
<div style="background:black;color:white">
1. <b>JSP & Servlets:</b> JSP is a webpage scripting language that can generate dynamic content 
while Servlets are Java programs that are already compiled which also creates dynamic web content.
Servlets run faster compared to JSP.<br>

2. <b>Spring:</b> Spring is the most popular application development framework for enterprise Java.
we use Spring Framework to create high performing, easily testable, and reusable code.<br>

3. <b>JDBC:</b> Java Database Connectivity (JDBC) is an application programming interface 
(API) for the programming language Java, which defines how a client may access a database. 
It is part of the Java Standard Edition platform, from Oracle Corporation.<br>

4. <b>Hibernate :</b> Hibernate framework simplifies the development of java application 
to interact with the database.<br>
</div>
    </section>
    </section>
    <section id="j47">
<h1> Java Card</h1>
This edition was targeted, to run applets smoothly and securely on smart cards and similar technology.<br>

__<br>

Java Card<br>


Java Card is an industry-standard technology platform developed by Sun Microsystems 
(now Oracle) to enable Java-based applications - applets - to run on smart cards.<br>

Java Card helps developers build, test, and deploy smart card-based applications 
quickly and efficiently with an object-oriented programming model <br>


Java Card technology is used in a wide range of smart card applications,<br>


Smart ID badges<br>

SIM<br>

Government id & health card<br>

    </section>
    <section id="j48">
<h1> Java applications in realtime</h1><br>
Android SDK uses java<br>
(#1) Desktop GUI Applications<br>
(#2) Web Applications<br>
(#3) Mobile Applications<br>
(#4) Enterprise Applications<br>
(#5) Scientific Applications<br>
(#6) Web Servers & Applications Servers<br>
(#7) Embedded Systems<br>
(#8) Server Apps In Financial Industry<br>
(#9) Software Tools<br>
(#10) Trading Applications<br>
(#11) J2ME Apps<br>
(#12) Big Data Technologies<br>

    </section>
    <section id="j49">
<h1>  Frameworks</h1>
Framework is a layered structure built on top of a programming language. 
For example, Rails, also known as <b>Ruby on Rails</b>, is a web framework 
built on top of the Ruby programming language.Collection of packages or 
modules that allows us to perform a task or service. <br>
<b>frameworks are software libraries</b> created to make building  
applications easier and faster.<br>
It is often a layered structure indicating what kind of programs can or should be built and how they would interrelate. 
    </section>
    <section id="j50">
<h1>Diamond Warning<> [Java 7]</h1><br>
It came to simplify the use of generics when creating object. It avoids unchecked warnings &  make the program more readable. 
The diamond operator is a nice feature as you don't have to repeat yourself. It makes sense to define the type 
once when you declare the type but just doesn't make sense to define it again on the right side.<br>
The point for diamond operator is simply to reduce typing of code when declaring generic types.
It doesn't have any effect on runtime whatsoever.<br>
The only difference if you specify in Java 5 and 6,<br>

List<String> list = new ArrayList();<br>
is that you have to specify @SuppressWarnings("unchecked") to the list 
(otherwise you will get an unchecked cast warning). My understanding is 
that diamond operator is trying to make development easier. It's got 
nothing to do on runtime execution of generics at all.
    </section>
    <section id="j51">
<h1>Sealed class [JDK 15]</h1><br>
Restrict any class to be sub class of any other class without  final keyword by using sealed keyword .
Java sealed classes and interfaces restrict that which classes and interfaces may extend or implement 
them. In other words, we can say that the class that cannot be inherited but can be instantiated is 
known as the sealed class.<br>
we can seal classes by applying the same sealed modifier. The permits clause should 
be defined after any extends or implements clauses:<br>
public abstract sealed class Vehicle permits Car, Truck {<br>
    protected final String registrationNumber;<br>
    public Vehicle(String registrationNumber) {<br>
        this.registrationNumber = registrationNumber;<br>
    }<br>
    public String getRegistrationNumber() {<br>
        return registrationNumber;<br>
    }<br>
}<br>

    </section> 
    <section id="sealed" style="font-size:10px">
<h1>Sealed class Example</h1><br>
A permitted subclass must define a modifier. It may be declared final to prevent any further extensions:<br>
public final class Truck extends Vehicle implements Service {<br>
    private final int loadCapacity;<br>
    public Truck(int loadCapacity, String registrationNumber) {<br>
        super(registrationNumber);<br>
        this.loadCapacity = loadCapacity;<br>
    }<br>
    public int getLoadCapacity() {<br>
        return loadCapacity;<br>
    }<br>
    @Override<br>
    public int getMaxServiceIntervalInMonths() {<br>
        return 18;<br>
    }<br>
}<br>
A permitted subclass may also be declared sealed. However, if we declare it non-sealed, then it is open for extension:<br>
public non-sealed class Car extends Vehicle implements Service {<br>
    private final int numberOfSeats;<br>
    public Car(int numberOfSeats, String registrationNumber) {<br>
        super(registrationNumber);<br>
        this.numberOfSeats = numberOfSeats;<br>
    }<br>
    public int getNumberOfSeats() {<br>
        return numberOfSeats;<br>
    }<br>
    @Override<br>
    public int getMaxServiceIntervalInMonths() {<br>
        return 12;<br>
    }<br>
}<br>

    </section>     
    <section id="j52">
<h1>Annotation in Java</h1><br>
Annotations( a form of metadata , conveys some special meanings to the compiler) 
became available in Java since JDK 1.5.<br>
With annotation , the user can get an alert or warning incase we write 
something wrong or missing anything to write.Such annotation Expression
will become prevalent at compile-time.It behaves same as comment but also
reveal the intention of the user to the computer that what he actually
want to do if he has committed some error.<br>
<b>eg... @override, @suppresswarning etc.
    </section>   
    <section id="j53">
<h1>Marker Interface</h1><br>
<b>Marker interface -</b> An interface that does not contain methods, fields, and 
constants is known as marker interface.

It delivers the run-time type information about an object. It is the reason 
that the JVM and compiler have additional information about an object. it

inform the Java compiler by a message so that it can add some special behavior 
to the class implementing it. <br>

<b>__________</b><br>

<b>clone() of cloneable marker interface -</b> It generates replica (copy) of an 
object with different name.<br>

(eg... crudrepotory,jparepository r marker interface)
built-in marker interfaces are the interfaces that are already present in 
the JDK and ready to use.<br>
#1 Serializable interface: Serializable is a marker interface present in 
the java.io package. We can serialize objects using this interface i.e. save 
the object state into a file. Serialization can be defined as a process by 
which we convert the object state into its equivalent byte stream to store 
the object into the memory in a file or persist the object.<br>


When we want to retrieve the object from its saved state and access its contents, 
we will have to convert the byte stream back to the actual Java object and 
his process is called deserialization.<br>



The entire mechanism of serialization and deserialization is platform-independent. 
This means that we can serialize the object on one platform and then deserialize 
it on a different platform.<br>


When we say that we have serialized the Java object, it means that we have called 
the ObjectOutputStream method writeObject () to write the object to a file.<br>


Similarly,

in the case of deserialization, we call the ObjectInputStream:: readObject () 
method to read the data from the file that has stored the object.<br>

 If a particular field is not serializable then we should mark it as transient.<br>

Java.io.NotSerializableException<br>

thrown when the class is not eligible for serialization. The class that does not 
implement the Serializable interface becomes ineligible for serialization.<br>

Serialization (writing) <br>
deserialization (reading)<br>

<b>#2 Cloneable interface:</b> The cloneable interface is a part of the java.lang package 
and allows the objects to be cloned. Object cloning is a process using which we 
create an exact copy of the object using the clone () method of the Object class<br>

The <b>clone ()</b> method creates an exact copy of the object with less processing time 
than that taken for creating a new object using the new keyword.<br>

<b>CloneNotSupportedException :</b> If a class does not implement a Cloneable interface 
and still invokes the clone () method,<br>


When the one-dimensional array is cloned, a deep copy of the array is generated. 
When a 2-dimensional array is cloned, then a shallow copy is made.<br>

Making a shallow copy is the default behavior of the clone () method in Java.<br>

In deep cloning, we make a copy of the object that is independent of the original 
object. Any changes then made to the clone object will not reflect in the original . <br>

The default implementation of the clone () method creates a shallow copy of the 
object while we can override the clone () method to create a deep copy.<br>


<b>#3 Remote interface:</b> The remote interface is a part of the java.RMI package and 
we use this interface to create RMI applications
It marks an object as remote that can be accessed from another machine (host).<br>

<b>__________</b><br>

<b>How annotations can be use as a replacement to marker interface</b><br>

As marker interfaces are mostly useful for just marking a class, the same thing 
can be achievable through annotations. Marker interfaces are better than 
annotations when they're used to define a type. For example, Serializable 
can be used as the type of an argument that must be serializable.<br>

public void writeToFile(Serializable object);<br>
If the marker interface doesn't define a type, but only meta-data, then an annotation is better.<br>

    </section>  
</div> </div>
<script type="text/javascript">
$(document).ready(function(){
$('.sub-btn').click(function(){
    $(this).next('.sub-menu').slideToggle();
    $(this).find('.dropdown').toggleClass('rotate');
});

});

</script>
 <a href="<?= base_url().'Welcome/test3'?>" class="float">
<i class="fas fa-info-circle"></i>
</a>
  </body>
</html>