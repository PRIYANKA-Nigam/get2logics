
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Jenkins Blogs</title>

 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" 
    integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" 
    crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" 
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
        <style>
        body{
	font-family:Verdana, Geneva, sans-serif;
	font-size:15px;
	background-color:#CCC;
}

.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.fas{
	margin-top:18px;
	font-size: 2em;
}   
#icon{
  width: 30px;
  cursor: pointer;
}
:root{
  --primary-color:#edf2fc;
  --secondary-color:white;
}
.dark-theme{
  --primary-color:yellow;
  --secondary-color:red;
}
.g{
  background: var(--primary-color);
}
.gtl{
  background: var(--secondary-color);
}
body{
  background: var(--primary-color);
}
table, th, td {
  border: 1px solid black;
  padding:4px;
}
      </style>
</head>

<body id="page-top">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" 
    crossorigin="anonymous"></script>
    <div class="container">
    <div id="google_element" style="float:right"></div>
  <script src="https://translate.google.com/translate_a/element.js?cb=loadGoogleTranslate"></script>
        <script>
           function loadGoogleTranslate(){
            new google.translate.TranslateElement("google_element");
           }
            </script>
             <img src='<?=base_url().'setting.jpg'?>' id="icon" width="25px" height="25px" >
    <a href="<?=base_url().'Welcome/Blog'?>" ><button class="btn btn-success" >Back</button></a>
        <h1>Jenkins</h1><hr><br>
    <div class="row">

<div class="col-md-6">
    <div class="card border-left-primary shadow h-100 py-2 ">
        <div class="card-body ">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                <div class="h5 mb-0 font-weight-bold text-gray-800">Jenkins</div><br>
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 gtl">
                        Jenkins is an open source automation tool written in java with plugins
                        built for continuous integration process.It is used to build and test 
                        software projects continuously making it easier for developers to integrate
                        changes to projects.It is used to build and test our project continuously.<br><br>
                    Jenkins is used for implementing CI/CD pipelines. 
                    These pipelines automate the testing and integrate the changes 
                    from other branches to the main branch .<br><br>
Jenkins work as -<br>
<ul>
<li>Developers make the necessary changes and commit them to the source code.</li>
<li>The repository is continuously checked by Jenkins for any changes.</li>
<li>If the build process is successful, an executable is generated.</li>
<li>If no issues are found, it gets deployed to the production server</li>
</ul><br>

Once the software is rolled out in the market, it is constantly under the 
scanner, meaning that the product is bound to receive some feature update 
or a certain version upgrade .<br>
This entire process of integrating changes, testing, and releasing is 
termed as ‘Continuous Integration’ and ‘Continuous Development’.
<br>
Jenkins followed a CI/CD pipeline(Steps under CI/CD) -<br>
<b>CI :</b><br>
<ul>
    <li>Push code from local to a git repo.</li>
    <li>Create a build pipleline that will take our code from repo 
    as input and create a docker image and pushed it to Azure container
    registry.</li>
</ul><br>
<b>CD :</b><br>
<ul>
    <li>we need to create a release pipeline that will take docker image
    from the Azure container registry and create Azure container instances.</li>
</ul>
<br>
<b><i>Azure Container Registry is a hosting platform for docker image.Such
images can be used for container based deployment in any platform.</i></b>
</div>
                  
                </div>
               
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="card border-left-primary shadow h-100 py-2 ">
        <div class="card-body ">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                <div class="h5 mb-0 font-weight-bold text-gray-800">Code Flow with Continuous Integration</div><br>
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 gtl">
                 <b>Problems Without Continuous Integration</b><br>
                 Suppose there is a developer who is waiting for a long time inorder to get the test result of the 
                 deployed code.1st the entire source code of the application will built and than only it will be 
                 deployed to the test server for testing.It takes a lot of time .So, developers have to wait for a long time inorder to get 
                 the test result.<br>
                 Since,entire code of application is 1st built and than it is tested.So, if their is any bug in the
                 code ,developers have to go through entire source code of application,So, it is a headache for developers
                 as the code that was built successfully but in testing there was certain bugs.
                 So,he/she has to go through the entire source code to detect and remove that bug.
                 Here, locating and fixing the bug is a very time-taking process.Developers have
                 to waste a lot of time in locating and fixing the bug than implementing any new feature.<br><br>
                 <b>Code mechanism with COntinuous Integration </b><br>
                 If there are multiple developers working on any code and any one of them makes any commit
                 to the source code ,the code will be pulled from the repository.It will be built,tested and
                 deploy.So, here is the advantage ,as the developers here will be able to check which
                 commit Id has causes bug and can be treated there itself.So,there is no need to go 
                 through entire code of application they just need the particular commit which caused bug.
                 So, this way locating and fixing of bug becomes very easy.<br><br>
                 <table border=2>
                     <tr>
                        <th>S.NO</th> <th>Before Continuous Integration</th><th>After Continuous Integration</th>
                     </tr>
                     <tr>
                       <td>1.</td>  <td>The entire source code was built and than tested</td><td>
                             Every commit made in source code is built and tested
                         </td>
                     </tr>
                     <tr>
                      <td>2.</td>   <td>Developers have to wait for test results</td><td>Test results of every commit made in source code is known</td>
                     </tr>
                     <tr>
                      <td>3.</td>   <td>No feedback</td><td>Feedback is there</td>
                     </tr>
                 </table>
                 .</div>
                  
                </div>
               
            </div>
        </div>
    </div>
</div>
<br><br>
  <div class="row">

<div class="col-md-6 ">
    <div class="card border-left-success shadow h-100 py-2 ">
        <div class="card-body ">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                <div class="h5 mb-0 font-weight-bold text-gray-800">CI/CD Pipeline</div><br>
                </div>
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1 gtl">
            CI - Continuous Integration<br>
            CD - Continuous Delivery/Continuous Deployment<br>
            The process involved in a project in an orgranisation are -
            <ul>
                <li>Dev</li>
                <li>Testing</li>
                <li>Deployment/Release</li>
            </ul><br>
            <b>Continuous Integration -</b>This process happens mostly in Dev Phase.In this process,
            team members integrate their code or their work in a shared repository and this process 
            is best achieve by some source code management tool like - Git.So,they integrate their work
            on a regular basis.Members in dev team will integrate their work and check in the shared
            repository and then this check-in is than followed and validated by an automated build and 
            automated unit test.<br>
            This process expands from dev phase to some parts of testing phase as most of the test are 
            unit test which are automated at the dev end.<br>
            <b>Under this phase, we run some unit test.</b><br><br>
            <b>Continuous Delivery -</b> After CI process ,there is requirement of deployment in an 
            environment that can be a QA or can be a staging environment and than some automated test
            are run to ensure that the code is now ready to be deployed.Goal of this process is always
            to make sure that build is always in a deployable state or a deployment ready state.<br>
            Here occures deployment in pre-prod or staging environment not actual environment.<br>
            It expands from dev phase to the entire testing phase till the staging environment.So,here
            goal is that our build or code is now ready to be deployed on any environment.<br>
            <b>Staging Environment </b>is like a production like environment and goal here is that we
            keep a environment exactly like a copy of our production environment so that before deploying 
            anyrhing on the production we can check it on the pre-prod or staging environment.And also if
            we want to check anything instead of going to actual prod environmennt we can check it on the staging or pre-
            prod environment.<br>
            <b>Continuous Deployment -</b>Here Deployment takes place in the actual prod environment.Here along
            with CI and continuous delivery , we also do production deployment.So,this automated deployment to
            production and every change,every release that passes through all the automated test is than directly
            deployed to production environment.This process is not done automatically but there are organisation 
            who do very frequent and very fast deployment.
            <br>
            <table border=3>
                <tr>
                    <th>Stages</th><th>Build</th><th>Test</th><th>Deploy</th><th>Release</th>
                </tr>
                <tr>
                    <th>Continuous Integration</th><td>✔</td><td> ✔ (till QA phase)</td><td></td><td></td>
                </tr>
                <tr>
                    <th>Continuous Delivery</th><td>✔ </td><td> ✔</td><td> ✔ (till  staging environment)</td><td> </td>
                </tr>
                <tr>
                    <th>Continuous Deployment</th><td>✔ </td><td> ✔</td><td> ✔</td><td>  ✔   (till end stage of production)</td>
                </tr>
            </table>
            <br>
            If we chain all these process through an automated trigger,it will be CI/CD Pipeline.
            <br>
 </div>
                   
               
            </div>
        </div>
    </div>
</div>
<div class="col-md-6 ">
    <div class="card border-left-success shadow h-100 py-2 ">
        <div class="card-body ">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                <div class="h5 mb-0 font-weight-bold text-gray-800">Jenkins Vs Azure DevOps</div><br>
                </div>
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1 gtl">
                   <b> Azure DevOps </b> is  A cloud-based repository a cloud-based CI tool by Microsoft. 
                    It can be understood as a Software-as-a-Service (SaaS) platform,. It Offers a 
                    platform for containers and Kubernetes<br>
                    <b>Azure Pipelines - </b> Azure Pipeline is used for testing, building, managing, 
                    and deploying applications. It is a cloud service that is readily available for 
                    you to build and test your code project.<br>
<b>Group Tasks – </b> allows you to perform a sequence of tasks, already defined in a pipeline, into a 
single task, whereas Jenkins is generally done by a single user which leads to tracking and accountability problems.<br>
<b>YAML Interface –</b> With YAML in Azure Pipelines, you can configure CI/CD pipeline as code, whereas Jenkins doesn’t 
have a YAML interface.<br>



Jenkins is more flexible to create and deploy complex workflows, Azure DevOps is faster to adapt. 
In most cases, organizations use both the tools and in such cases, Azure Pipelines supports integration with Jenkins
As azure DevOps is cloud base  so it Offers the latest features with frequent updates, 
Gives higher customer satisfaction than jenkins or bamboo.<br>

<b>Does Azure DevOps have jenkins --- </b><br>
Azure Pipelines supports integration with Jenkins so that you can use Jenkins for Continuous 
Integration (CI) while gaining several DevOps benefits from an Azure Pipelines 
release pipeline that deploys to Azure.<br>
<b>Code Migrate from jenkins to DevOps --- </b><br>
In Azure DevOps project, go to Project Settings -> Service Hooks. Then select 
Create Subscription and choose Jenkins. Use the “Code pushed” event and set 
any specific filters as necessary for</div>
                   
              
            </div>
        </div>
    </div>
</div>

</div>
    </div> <br>
    <div class="row">

<!-- Earnings (Monthly) Card Example -->
<div class="col-md-6">
    <div class="card border-left-primary shadow h-100 py-2 ">
        <div class="card-body ">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                <div class="h5 mb-0 font-weight-bold text-gray-800">Jenkins Vs Bamboo</div><br>
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 gtl">
                    Jenkins’ integration with Jira and Bitbucket is limited and the process 
                    requires additional components in the configuration which takes time and
                     labour. With Bamboo, basic configuration options are already built-in. 
                     There are several plug-ins that enable Jenkins to integrate with Jira.<br>
                    Jenkins has been a major open-source player for a long time and has 
                    a huge user base because of it. But Bamboo just like other Atlassian 
                    products has much more in-built capabilities and is easier to use. 
                    Jenkins is open-source, and Bamboo is a commercial CI/CD tool that 
                    can be levied against a subscription fee.<br>

While both the tools have their own advantages, however, it ultimately depends on your 
project requirements, budget, and timelines to choose the CI/CD tool for your project.</div>
                  
                </div>
                
            </div>
        </div>
    </div>
</div>

<div class="col-md-6 ">
    <div class="card border-left-success shadow h-100 py-2 ">
        <div class="card-body ">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                <div class="h5 mb-0 font-weight-bold text-gray-800">Bamboo</div><br>
                </div>
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1 gtl">
                    Bamboo is an automation server used for Continuous Integration. Developed by Atlassian 
                    in 2007, this tool allows the developers to automatically build, document, integrate, 
                    test the source code and prepare an app for deployment.<br>
Most of Bamboo software functionality is pre-built. In other words, this product doesn’t 
require plug-ins. When it comes to Bamboo continuous integration, it also has built-in deployment 
that mean it is seamlessly integrated with products such as Bitbucket and Jira.<br>
When it comes to Bamboo vs Jenkins  user-interface, Bamboo’s is tidier and more intuitive. 
Each time a new task is added, it provides more help and guidance through the plan’s build 
and deployment stages. The biggest difference between Bamboo vs Jenkins is that Jenkins is
Open Source – which means it’s free<br>
 </div>
                   
               
            </div>
        </div>
    </div>
</div>

    </div>
</div>
<script> 
     var icon=document.getElementById("icon");
     icon.onclick =function(){
      document.body.classList.toggle("dark-theme");
      if(document.body.classList.contains("dark-theme")){
        icon.src="<?=base_url().'setting.jpg'?>";
      }else{
        icon.src="<?=base_url().'moon.jpg'?>";
      }
     }
     </script>  
      <a href="<?= base_url().'Welcome/test3'?>" class="float">
<i class="fas fa-info-circle"></i>
</a>
</body></html>
