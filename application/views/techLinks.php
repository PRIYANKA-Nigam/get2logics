
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Technical Code & Links</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" 
    integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" 
    crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" 
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.3/examples/sign-in/">
 <link rel="stylesheet" type="text/css" href="https://getbootstrap.com/docs/5.3/examples/dashboard/dashboard.css">
   <style> 
   body{
	font-family:Verdana, Geneva, sans-serif;
	font-size:18px;
	background-color:#CCC;
}

.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.fas{
	margin-top:18px;
	font-size: 2em;
} 
 body{
  background: var(--primary-color);
  max-height: 100vh; 
  /* by giving max-height: 100vh the footer got stick to the bottom of the page */
}
</style>    

<link href="/docs/5.3/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>
<body id="page-top" >
        <!-- Content Wrapper -->
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" 
    crossorigin="anonymous"></script>


<div class="col-lg-12 mb-4">
                           <div class="card shadow mb-4">
                          <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">APK Files & Code</h6>
                                   
                                </div>
                                <div id="google_element" style="float:right"></div>
  <script src="http://translate.google.com/translate_a/element.js?cb=loadGoogleTranslate"></script>
        <script>
           function loadGoogleTranslate(){
            new google.translate.TranslateElement("google_element");
           }
            </script>
                                <a href="<?=base_url().'Welcome/'?>" ><button class="btn btn-primary" >Back</button></a> 
                                <!-- Card Body -->
                                <div class="card-body">
                           <div class="album py-5 bg-light">
                              <div class="container">
                                 <div class="row">
                                    <h1>Android</h1>
                                 <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <h4 class="card-text"><u>Novel Scholar App</u></h4>
              <div class="d-flex justify-content-between align-items-center">
                              <div class="container">
                                <div class="row">
                                 <label style="margin-top:10px;"> App:  <a href="https://drive.google.com/file/d/1_gPLKAwDxVMyisNBzG7vF5VWn9LLSoil/view?usp=share_link">Novel Scholar APK file</a></label>
                                  <label>Zip:  <a href="https://drive.google.com/file/d/1ZkblVzsUXxeX9RqHbhOxgt738xtK9W5F/view?usp=share_link">Novel Scholar Zip file</a></label>
                                <a href="<?= base_url().'Welcome/AppPreview?name=novel'?>" ><button class="btn btn-primary" style="margin-left: 100px;margin-top:30px;">Preview</button></a>
                                </div>
                              </div>
                         </div></div></div></div>
            <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <p class="card-text"><u>Greetings App</u></p>
              <div class="d-flex justify-content-between align-items-center">
              <div class="container">
                                <div class="row">
                                 <label style="margin-top:10px;"> App:  <a href="https://drive.google.com/file/d/1iXjXp30p2Z8g7Ld4Mvpr-64Cd4OPEGE_/view?usp=share_link">Greetings APK file</a></label>
                                  <label>Zip:  <a href="https://drive.google.com/file/d/1AkKHHKJmdsPGFMgEp5dRCN1YWjSpoL9r/view?usp=share_link">Greetings Zip file</a></label>
                                <a href="<?= base_url().'Welcome/AppPreview?name=greetings'?>" ><button class="btn btn-primary" style="margin-left: 100px;margin-top:30px;">Preview</button></a>
                                </div>
                              </div>
                                    </div> </div></div></div>
                                    <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <p class="card-text">Quiz App</p>
              <div class="d-flex justify-content-between align-items-center">
              <div class="container">
                                <div class="row">
                                 <label style="margin-top:10px;"> App:  <a href="https://drive.google.com/file/d/1bLyZvfsX85yqMRSqfAIov8O-yAdXWJYw/view?usp=share_link">Quiz APK file</a></label>
                                  <label>Zip:  <a href="https://drive.google.com/file/d/1n2emUpROF8c2ejabw-3BIZv8IHEL7V31/view?usp=share_link">Quiz Zip file</a></label>
                                <a href="<?= base_url().'Welcome/AppPreview?name=quiz'?>" ><button class="btn btn-primary" style="margin-left: 100px;margin-top:30px;">Preview</button></a>
                                </div>
                              </div>
                                    
                         </div></div></div></div>
            <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <p class="card-text">Shlok Mantra App</p>
              <div class="d-flex justify-content-between align-items-center">
              <div class="container">
                                <div class="row">
                                 <label style="margin-top:10px;"> App:  <a href="https://drive.google.com/file/d/177socPg2FjmvtkHTlFiLf2CdnF4LAscv/view?usp=share_link">Shlok Mantra APK file</a></label>
                                  <label>Zip:  <a href="https://drive.google.com/file/d/1rRRjFfZaiIU-xZBNjz9VKObCNmCqcNmV/view?usp=share_link">Shlok Mantra Zip file</a></label>
                                <a href="<?= base_url().'Welcome/AppPreview?name=shlok'?>" ><button class="btn btn-primary" style="margin-left: 100px;margin-top:30px;">Preview</button></a>
                                </div>
                              </div>
                                    </div> </div></div></div>
                                    <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <p class="card-text">Inventory Management App</p>
              <div class="d-flex justify-content-between align-items-center">
              <div class="container">
                                <div class="row">
                                 <label style="margin-top:10px;"> App:  <a href="https://drive.google.com/file/d/1Xi8vV-ckfmChioOtj8F_PQ7y4MEXXKxr/view?usp=share_link">Inventory Management APK file</a></label>
                                  <label>Zip:  <a href="https://drive.google.com/file/d/100NMk76nYclI8XNR18HsLMdweLH7iMBZ/view?usp=share_link">Inventory Management Zip file</a></label>
                                <a href="<?= base_url().'Welcome/AppPreview?name=inventory'?>" ><button class="btn btn-primary" style="margin-left: 100px;margin-top:30px;">Preview</button></a>
                                </div>
                              </div>
                                    
                         </div></div></div></div>
            <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <p class="card-text">Instance Convertor App</p>
              <div class="d-flex justify-content-between align-items-center">
              <div class="container">
                                <div class="row">
                                 <label style="margin-top:10px;"> App:  <a href="https://drive.google.com/file/d/14vAgcUD60_IdD-3blXNkQb56HKV7FMkc/view?usp=share_link">Instance Convertor APK file</a></label>
                                  <label>Zip:  <a href="https://drive.google.com/file/d/17XPMU1vTQAnT8daBrtU5uuKJFhCKcAL2/view?usp=share_link">Instance Convertor Zip file</a></label>
                                <a href="<?= base_url().'Welcome/AppPreview?name=convertor'?>" ><button class="btn btn-primary" style="margin-left: 100px;margin-top:30px;">Preview</button></a>
                                </div>
                              </div>
                                    </div> </div></div></div>
                                    <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <p class="card-text">Attendance App</p>
              <div class="d-flex justify-content-between align-items-center">
              <div class="container">
                                <div class="row">
                                 <label style="margin-top:10px;"> App:  <a href="https://drive.google.com/file/d/1sMYIFgF-Vz3WhWqs-1urAoajHss8CQWX/view?usp=share_link">Attendance APK file</a></label>
                                  <label>Zip:  <a href="https://drive.google.com/file/d/10xbujDxKND_3-Cs_CRLYSZ94_GdzvR-C/view?usp=share_link">Attendance Zip file</a></label>
                                <a href="<?= base_url().'Welcome/AppPreview?name=attendance'?>" ><button class="btn btn-primary" style="margin-left: 100px;margin-top:30px;">Preview</button></a>
                                </div>
                              </div>
                                    </div> </div></div></div>
                                    <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <p class="card-text">Notification App</p>
              <div class="d-flex justify-content-between align-items-center">
              <div class="container">
                                <div class="row">
                                 <label style="margin-top:10px;"> App:  <a href="https://drive.google.com/file/d/1MV3j20Ytk7_2FQJZ3Ua1bXyVH-Ruw9la/view?usp=share_link">Notification APK file</a></label>
                                  <label>Zip:  <a href="https://drive.google.com/file/d/1uYKNXo97pt3mACNfZSlAuXzdKQWKBKKb/view?usp=share_link">Notification Zip file</a></label>
                                <a href="<?= base_url().'Welcome/AppPreview?name=notification'?>" ><button class="btn btn-primary" style="margin-left: 100px;margin-top:30px;">Preview</button></a>
                                </div>
                              </div>
                                    </div> </div></div></div>
                                     <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <p class="card-text">Video Player App</p>
              <div class="d-flex justify-content-between align-items-center">
              <div class="container">
                                <div class="row">
                                 <label style="margin-top:10px;"> App:  <a href="https://drive.google.com/file/d/1RUJddm2y-C3Wg32KYH-Y-M29dvAc6yHH/view?usp=share_link">Video Player APK file</a></label>
                                  <label>Zip:  <a href="https://drive.google.com/file/d/1xoSMabOHBqqr5gKKkY73AXgcZIzTaUrE/view?usp=share_link">Video Player Zip file</a></label>
                                <a href="<?= base_url().'Welcome/AppPreview?name=video'?>" ><button class="btn btn-primary" style="margin-left: 100px;margin-top:30px;">Preview</button></a>
                                </div>
                              </div>
                                    </div> </div></div></div>
                                             <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <p class="card-text">JSON Parser App</p>
              <div class="d-flex justify-content-between align-items-center">
              <div class="container">
                                <div class="row">
                                 <label style="margin-top:10px;"> App:  <a href="https://drive.google.com/file/d/1YlPrL2Pl686wE2Ofvw_O9Fk0WRDA0eun/view?usp=share_link">JSON Parser APK file</a></label>
                                  <label>Zip:  <a href="https://drive.google.com/file/d/1-7DriuzywhSzSdl78EKiTWDz7Vprul0L/view?usp=share_link">JSON Parser Zip file</a></label>
                                <a href="<?= base_url().'Welcome/AppPreview?name=JSON'?>" ><button class="btn btn-primary" style="margin-left: 100px;margin-top:30px;">Preview</button></a>
                                </div>
                              </div>
                                    </div> </div></div></div>
                                    <div class="col-lg-12 mb-4">
                           <div class="card shadow mb-4">
                          <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Some important App Source Code</h6>
                                   
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                           <div class="album py-5 bg-light">
                              <div class="container">
                                 <div class="row">
                                 <div class="col-md-2">
                                  <b> S.No.</b>
                                  </div>
                                  <div class="col-md-4">
                                   <b> App Name</b>
                                  </div>
                                  <div class="col-md-6">
                                 <b style="margin-left:30px"> Source Code Link</b>
                                  </div>
                              </div>
                              <div class="row">
                               <fieldset><b style="margin-left:400px">Android</b></fieldset>
                               <hr>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                   1.
                                  </div>
                                  <div class="col-md-4">
                                   Novel Scholar App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/Novel-Scholar-App">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/Novel-Scholar-App" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  2.
                                  </div>
                                  <div class="col-md-4">
                                   Greetings App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/Greetings-App">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/Greetings-App" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  3.
                                  </div>
                                  <div class="col-md-4">
                                   Quiz App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/Quiz-App">App Github Link</a>
                                 <a href="#" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  4.
                                  </div>
                                  <div class="col-md-4">
                                   Shlok Mantra App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/Shlok-Mantra-App">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/Shlok-Mantra-App" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  5.
                                  </div>
                                  <div class="col-md-4">
                                  Inventory Management App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/InventoryManagement-App">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/InventoryManagement-App" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                 6.
                                  </div>
                                  <div class="col-md-4">
                                  Instance Convertor App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/Convertor-App">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/Convertor-App" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  7.
                                  </div>
                                  <div class="col-md-4">
                                  Attendance App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/Attendance-App">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/Attendance-App" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                 8.
                                  </div>
                                  <div class="col-md-4">
                                  Notification App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/Notification-APP">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/Notification-APP" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                 9.
                                  </div>
                                  <div class="col-md-4">
                                  Video Player App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/Video-Player-App">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/Video-Player-App" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                 10.
                                  </div>
                                  <div class="col-md-4">
                                  JSON Parser App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/Json-Parser-Volley">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/Json-Parser-Volley" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                               <fieldset><b style="margin-left:400px">PHP</b></fieldset>
                               <hr>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                 11.
                                  </div>
                                  <div class="col-md-4">
                                   CV Maker App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/email_PDF%20App">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/cv-maker-app" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                 12.
                                  </div>
                                  <div class="col-md-4">
                                  Banking App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/Banking%20App">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/banking-app" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                 13.
                                  </div>
                                  <div class="col-md-4">
                                  College Management  App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/CollegeManagementSystem/ci">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/college-management-system" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                14.
                                  </div>
                                  <div class="col-md-4">
                                  Dictionary App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/Dictionary%20App">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/dictionary-app" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                 15.
                                  </div>
                                  <div class="col-md-4">
                                   Shopping Cart App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/ShoppingCartApp/ci">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/shopping-cart-app" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                 16.
                                  </div>
                                  <div class="col-md-4">
                                  Language Translator App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/Dictionary%20App/languageTranslatorApp">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/language-translator-app" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                 17.
                                  </div>
                                  <div class="col-md-4">
                                  ATM Cash Dispenser App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/blob/master/Characters%20Manipulation%20App/ATM_Cash_Dispense.php">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/atm-cash-dispenser-app" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  18.
                                  </div>
                                  <div class="col-md-4">
                                  Monthlies Tracker App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/blob/master/Characters%20Manipulation%20App/Monthlies.php">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/monthlies-tracker-app" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                 19.
                                  </div>
                                  <div class="col-md-4">
                                   Calculator App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/calculator%20App">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/calculator-app" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  20.
                                  </div>
                                  <div class="col-md-4">
                                   Quiz App(With Normal PHP)
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/Quiz%20App">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/quiz-app" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  21.
                                  </div>
                                  <div class="col-md-4">
                                   PHP Basics App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/blob/master/Characters%20Manipulation%20App/character_Manipulation.php">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/php-basics" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  22.
                                  </div>
                                  <div class="col-md-4">
                                 API Creation in PHP
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/phpRest">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/post_get-api" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  23.
                                  </div>
                                  <div class="col-md-4">
                                   Database Manipulation App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/DB%20Manipulation%20App">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/database-search-operation" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  24.
                                  </div>
                                  <div class="col-md-4">
                                  MiniBlog App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/miniblog/ci">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/miniblog-app" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  25.
                                  </div>
                                  <div class="col-md-4">
                                 Multi Video View App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/blob/master/email_PDF%20App/video_upload.php">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/multi-video-view" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  26.
                                  </div>
                                  <div class="col-md-4">
                                 Multiple Image Upload App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/blob/master/email_PDF%20App/multiple_image_upload.php">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/multiple-image-upload" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  27.
                                  </div>
                                  <div class="col-md-4">
                                 Multiple PDF Upload
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/blob/master/email_PDF%20App/pdf_upload.php">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/multiple-pdf-upload" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  28.
                                  </div>
                                  <div class="col-md-4">
                                  Single PDF Upload
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/blob/master/email_PDF%20App/single_pdf_view.php">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/single-pdf-upload" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  29.
                                  </div>
                                  <div class="col-md-4">
                                 Weekly Schedule App
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/blob/master/Characters%20Manipulation%20App/firstPage.php">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/weekly-schedule-app" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  30.
                                  </div>
                                  <div class="col-md-4">
                                 Text To Json Convertor
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/Dictionary%20App/TextToJsonConvertorApp">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/text-to-json-convertor" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  31.
                                  </div>
                                  <div class="col-md-4">
                                 Quiz App(Using Codeigniter)
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/CodeIgniter">App Github Link</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/quiz-app-ci" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  32.
                                  </div>
                                  <div class="col-md-4">
                                Website Link
                                  </div>
                                  <div class="col-md-6">
                                 <a href="https://github.com/PRIYANKA-Nigam/PHP-Apps/tree/master/dummy/ci">App Github Link</a>
                                 <a href="#" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                               <fieldset><b style="margin-left:400px">Spring Boot</b></fieldset>
                               <hr>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  33.
                                  </div>
                                  <div class="col-md-4">
                                  Storing JSP form data to h2 in-memory database
                                  </div>
                                  <div class="col-md-6">
                                  <a href="https://gitlab.com/PRIYANKA-Nigam/spring-boot-apis/-/snippets/2533377">API Snippet</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/spring-boot-apis/-/blob/main/api_using_jsp_jpa_h2_in_memory_db.pdf" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  34.
                                  </div>
                                  <div class="col-md-4">
                                 Fetch stored data from h2 in-memory database using jpa repository
                                  </div>
                                  <div class="col-md-6">
                                  <a href="https://gitlab.com/PRIYANKA-Nigam/spring-boot-apis/-/snippets/2533380">API Snippet</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/spring-boot-apis/-/blob/main/get_api_using_jdbcTemplate_h2_in_memory_db-merged.pdf" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  35.
                                  </div>
                                  <div class="col-md-4">
                                Store data to h2 in-memory database using jdbctemplate 
                                  </div>
                                  <div class="col-md-6">
                                  <a href="https://gitlab.com/-/snippets/2533384">API Snippet</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/spring-boot-apis/-/blob/main/get_api_using_jdbcTemplate_h2_in_memory_db.pdf" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  36.
                                  </div>
                                  <div class="col-md-4">
                               CRUD operation with postman using ResponseEntity class
                                  </div>
                                  <div class="col-md-6">
                                  <a href="https://gitlab.com/-/snippets/2533386">API Snippet</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/spring-boot-apis/-/blob/main/post_get_api_with_responseEntity_class.pdf" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-2">
                                  37.
                                  </div>
                                  <div class="col-md-4">
                               Implementation of basic GET API 
                                  </div>
                                  <div class="col-md-6">
                                  <a href="https://gitlab.com/-/snippets/2533411">API Snippet</a>
                                 <a href="https://gitlab.com/PRIYANKA-Nigam/spring-boot-apis/-/blob/main/get_api_response.pdf" style="margin-left:40px">App Gitlab Link</a>
                                  </div>
                              </div>
                             
        </div>
            
                                </div>
                               
                            </div>
                        </div>
                    </div>
                    <?php 
        include('footer.php');
        ?>
          <a href="<?= base_url().'Welcome/test3'?>" class="float">
<i class="fas fa-info-circle"></i>
</a>
</body>
</html>                    