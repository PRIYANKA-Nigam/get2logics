<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Get2Logics Home Page</title>
    <style>
   *{padding:0;margin:0;}

body{
	font-family:Verdana, Geneva, sans-serif;
	font-size:18px;
	background-color:#CCC;
}

.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.fas{
	margin-top:18px;
	font-size: 2em;
}   
.dot{
    /* margin-left: 2dp; */
    height: 15px;
    width: 15px;
    background-color: black;
    border-radius: 50%;
    display: inline-block;
   /* margin-top: 50px; */
   
}
.dot1{
  height: 15px;
    width: 15px;
    background-color:white;
    border-radius: 50%;
    display: inline-block;
}
.a{
height: 35px;
width: 15px;
}
  
  img {
    max-width: 65%;
    max-height:10%;
    float: left;
  }
  .image2{
    max-width: 100%;
    max-height:10%;
    float: right;
    width: 60rem;
    /* margin-top: 40px; */
    /* margin-left: 60px; */
    /* padding-right: 5px; */
  
  
  }
  
  .text {
    font-size: 20px;
    padding-left: 40px;
    /* padding-top: 10%; */
    /* margin-top: 80px; */
    /* float: left; */
    display: inline-block;
    background-color: blueviolet;
    border-radius: 50%;
  }
  @media(prefers-color-scheme:dark){
    body{
      background-color: #222f3e;
    }
    .dot{
      background: #576574;
      color: #222f3e;
    }
  }
    </style>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" 
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" 
    integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="canonical" href="https://getbootstrap.com/docs/5.3/examples/sign-in/">
 <link rel="stylesheet" type="text/css" href="https://getbootstrap.com/docs/5.3/examples/dashboard/dashboard.css">
    

    

<link href="/docs/5.3/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
<style>
.mt-100{margin-top: 5px;}
.mt-30{margin-top: 30px;}
.mb-30{margin-bottom: 30px;}
</style>
</head>
<body id="page-top"> 
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" 
    crossorigin="anonymous"></script>
<div style="margin-left: 100px;">
<?php include('header2.php'); ?>
                    <div style="background-color:black;width:100%;height:200px; height:75%;min-height:5px;display:inline-block; white-space: nowrap;">
                    <p style="color:white;" class="gtl">///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////</p><br>
                   <p style="color:white;margin-top:10px" class="gtl">~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~</p><br>
                   <label style="color:white;margin-top:30px" class="gtl">//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////</label>
                    </div>
                 
                 
                    <div class="row" >
                      <div class="row">
                      <div class="col-md-8">
                        <div class="col-md-4">
                        <div class="text">
                <label >Confuse to choose right career path for yourself</label>
            </div>
                       
                        </div>
                        <div class="col-md-8">
         <div class="image">
                <img src="<?=base_url().'assets/images/carttonRight.jpg'?>" width="200px" height="200px">
                         </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="col-md-4">
                        <div class="text">
                <label>Here is your key.</label>
            </div>
                        </div>
                        <div class="col-md-8">
                         
            <div class="image2">
                <img src="<?=base_url().'assets/images/cartoonLeft.jpg'?>" width="350px" height="250px">
        </div></div>
                        </div>
                        </div>
                        <div class="row">

<!-- Content Column -->
<span class="dot"></span><span class="dot"></span>
<span class="dot a"><h4 class="gtl"><center><b style="color:white">Average Salary Path of Tech Roles</b></center></h4></span>
<span class="dot"></span><span class="dot"></span><br>
<div class="row">
<div class="col-md-7 mb-4">

    <!-- Project Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary ">Average Salary per domain</h6>
        </div>
        <div class="card-body ">
            <h4 class="small font-weight-bold ">DevOps Engineer <span
                    class="float-right">3-18.8 lakh</span></h4>
            <div class="progress mb-4">
                <div class="progress-bar bg-danger" role="progressbar" style="width: 60%"
                    aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h4 class="small font-weight-bold ">DevOps Engineer (Entry level)<span
                    class="float-right">3-7 lakh</span></h4>
            <div class="progress mb-4">
                <div class="progress-bar bg-warning" role="progressbar" style="width: 40%"
                    aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h4 class="small font-weight-bold">AI Engineer(Experienced) <span
                    class="float-right">10-21 lakh</span></h4>
            <div class="progress mb-4">
                <div class="progress-bar bg-success" role="progressbar" style="width: 100%"
                    aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h4 class="small font-weight-bold">AI Engineer <span
                    class="float-right">5-12 lakh</span></h4>
            <div class="progress mb-4">
                <div class="progress-bar bg-dark" role="progressbar" style="width: 90%"
                    aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h4 class="small font-weight-bold">ML Engineer(Senior) <span
                    class="float-right">6.5-20 lakh</span></h4>
            <div class="progress mb-4">
                <div class="progress-bar bg-alert" role="progressbar" style="width: 80%"
                    aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h4 class="small font-weight-bold">ML Engineer(Junior) <span
                    class="float-right">3-10 lakh</span></h4>
            <div class="progress mb-4">
                <div class="progress-bar bg-danger" role="progressbar" style="width: 60%"
                    aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h4 class="small font-weight-bold">Data Science Engineer<span
                    class="float-right">3-10 lakh</span></h4>
            <div class="progress mb-4">
                <div class="progress-bar bg-info" role="progressbar" style="width: 50%"
                    aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h4 class="small font-weight-bold">Big Data Engineer <span
                    class="float-right">13-20 lakh</span></h4>
            <div class="progress">
                <div class="progress-bar bg-dark" role="progressbar" style="width: 70%"
                    aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
            </div><br>
            <h4 class="small font-weight-bold">Mobile App Developer(Fresher)<span
                    class="float-right">3-6 lakh</span></h4>
            <div class="progress mb-4">
                <div class="progress-bar bg-success" role="progressbar" style="width: 60%"
                    aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h4 class="small font-weight-bold">Mobile App Developer(Experienced)<span
                    class="float-right">5-12 lakh</span></h4>
            <div class="progress mb-4">
                <div class="progress-bar bg-alert" role="progressbar" style="width: 67%"
                    aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h4 class="small font-weight-bold">Web Developer(Entry level) <span
                    class="float-right">3-7 lakh</span></h4>
            <div class="progress">
                <div class="progress-bar bg-warning" role="progressbar" style="width: 50%"
                    aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <h4 class="small font-weight-bold">Web Developer(Senior) <span
                    class="float-right">7-13 lakh</span></h4>
            <div class="progress">
                <div class="progress-bar bg-info" role="progressbar" style="width: 70%"
                    aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
             </div>
            
        </div>
        
</div>
<div class="col-lg-2" style="background:black;height:820px;width:350px;border-radius:20%">
 <label style="color:white;margin-top:5px;font-size:20px;margin-left:40px" class="gtl"><b>Experience</b></label><br><br><br>
 <div>
 <span class="dot1"></span> 
   <span class="dot1"></span>
      <span class="dot1"></span>  
       <span class="dot1"></span> <label style="color:white;margin-left:12px" class="gtl">4+ years ~~~~~~~~~~~~~~~</lebel></div><br><br>
       <div>
       <span class="dot1"></span> 
   <span class="dot1"></span>
      <span class="dot1"></span>  
       <span class="dot1"></span> <label style="color:white;margin-left:12px" class="gtl">0-3 years ~~~~~~~~~~~~~~~</lebel></div><br><br>
       <div>
       <span class="dot1"></span> 
   <span class="dot1"></span>
      <span class="dot1"></span>  
       <span class="dot1"></span> <label style="color:white;margin-left:12px" class="gtl">5+ years ~~~~~~~~~~~~~~~</lebel></div><br>
       <div>
       <span class="dot1"></span> 
   <span class="dot1"></span>
      <span class="dot1"></span>  
       <span class="dot1"></span> <label style="color:white;margin-left:12px" class="gtl">0-5 years ~~~~~~~~~~~~~~~</lebel></div><br><br>
       <div>
       <span class="dot1"></span> 
   <span class="dot1"></span>
      <span class="dot1"></span>  
       <span class="dot1"></span> <label style="color:white;margin-left:12px" class="gtl">5+ years ~~~~~~~~~~~~~~~</lebel></div><br><br>
       <div>
       <span class="dot1"></span> 
   <span class="dot1"></span>
      <span class="dot1"></span>  
       <span class="dot1"></span> <label style="color:white;margin-left:12px" class="gtl">0-4 years ~~~~~~~~~~~~~~~</lebel></div><br><br>
       <div>
       <span class="dot1"></span> 
   <span class="dot1"></span>
      <span class="dot1"></span>  
       <span class="dot1"></span> <label style="color:white;margin-left:12px" class="gtl">0-3 years ~~~~~~~~~~~~~~~</lebel></div><br>
       <div>
       <span class="dot1"></span> 
   <span class="dot1"></span>
      <span class="dot1"></span>  
       <span class="dot1"></span> <label style="color:white;margin-left:12px" class="gtl">5+ years ~~~~~~~~~~~~~~~</lebel></div><br><br>
       <div>
       <span class="dot1"></span> 
   <span class="dot1"></span>
      <span class="dot1"></span>  
       <span class="dot1"></span> <label style="color:white;margin-left:12px" class="gtl">0-4 years ~~~~~~~~~~~~~~~</lebel></div><br><br>
       <div>
       <span class="dot1"></span> 
   <span class="dot1"></span>
      <span class="dot1"></span>  
       <span class="dot1"></span> <label style="color:white;margin-left:12px" class="gtl">4+ years ~~~~~~~~~~~~~~~</lebel></div><br>
       <div>
       <span class="dot1"></span> 
   <span class="dot1"></span>
      <span class="dot1"></span>  
       <span class="dot1"></span> <label style="color:white;margin-left:12px" class="gtl">0-4 years ~~~~~~~~~~~~~~~</lebel></div><br>
       <div>
       <span class="dot1"></span> 
   <span class="dot1"></span>
      <span class="dot1"></span>  
       <span class="dot1"></span> <label style="color:white;margin-left:12px" class="gtl">5+ years ~~~~~~~~~~~~~~~</lebel></div><br><br>
</div>

    </div>
    <section id="video">
               
                         <div class="col-lg-12 mb-4">

<!-- Project Card Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Videos</h6>
    </div>
    <div class="card-body">
    <div class="album py-5 bg-light">
    <div class="container mt-100">
<h2 class="mb-30">Videos Details</h2>
        <ul class="list-group sidebar-nav">
<div class="row">


          
        <?php
        $start=0; $current_page=1;
        $per_page=3; 
        $record=count($res);  
        $page=ceil($record/$per_page)+1;
        if(isset($_GET['start'])){
          $start = $_GET['start'];
          if($start<=0){
            $start=0;
            $current_page=1;
          }else{
            $current_page=$start;
            $start--;
            $start = $start*$per_page;
          }
         
        }
       $sql ="select * from  tutorials limit $start,$per_page";
       $query = $this->db->query($sql);
       if($query->num_rows()>0){
        foreach($query->result() as $value){?>
          
          <div class="col-md-6">       
<li class="list-group-item"> <?php
              $data=$value->url;
    $final=str_replace('watch?v=','embed/',$data);
    echo "
    <iframe src='$final' 
    frameborder='0'
    allow='autoplay:encrypted-media'
    allowfullscreen>
    </iframe>
    ";
    ?></li> </div>
     <div class="col-md-6">    
    <li><?=$value->title?>
    </li>  
     </div>  
    
            <?php  } }else { ?>
             No records Found !!
              <?php }?>
         
          
            </div>
      </ul> 
       <ul class="pagination mt-30" >
        <?php 
        for($i=1;$i<=$page;$i++){ 
          $class='';
          if($current_page==$i){
            $class='active';
          }
          ?>
        <li class="page-item <?php echo $class?>"><a class="page-link" href="?start=<?php echo $i?>"><?=$i?></a></li>
      <?php } ?>
    
       </ul>
  
  </div>
        </div>
        </div>
        </div>
    </div>
</div>
        </section>
        <section id="links">
                          <div class="col-lg-12 mb-4">
                           <div class="card shadow mb-4">
                          <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">APK Files & Code</h6>
                                   
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                           <div class="album py-5 bg-light">
                           <div class="container mt-100">
<h2 class="mb-30">Source Code</h2>
        <ul class="list-group sidebar-nav">
<div class="row">  
        <?php
        $starts=0; $current_page=1;
        $per_page=4; 
        $record=count($res);  
        $page=ceil($record/$per_page);
      
        if(isset($_GET['starts'])){
          $start = $_GET['starts'];
          if($start<=0){
            $start=0;
            $current_page=1;
          }else{
            $current_page=$start;
            $start--;
            $start = $start*$per_page;
          }
         
        }
       $sql ="select * from  techlinks limit $start,$per_page";
       $query = $this->db->query($sql);
       if($query->num_rows()>0){
        foreach($query->result() as $value){?>
           <div class="col-md-4">    
    <li class="list-group-item active"><?php  echo $value->title ?>
    </li> <br> 
     </div>  
          <div class="col-md-8">       
<li class="list-group-item"> <?php
$p=$value->url;
printf('<a href="%1$s">%1$s</a>',htmlspecialchars($p,ENT_QUOTES));
    //   echo "<a href='//$p'>click here";
    //   echo '</a>';
   
    ?></li><br> </div>
    
    
            <?php  } }else { ?>
             No records Found !!
              <?php }?>
         
          
            </div>
      </ul> 
       <ul class="pagination mt-30" >
        <?php 
        for($i=1;$i<=$page;$i++){ 
          $class='';
          if($current_page==$i){
            $class='active';
          }
          ?>
        <li class="page-item <?php echo $class?>"><a class="page-link" href="?starts=<?php echo $i?>"><?=$i?></a></li>
      <?php } ?>
    
       </ul>
  
  </div>
        </div>
                                    
        </div>
        </div>
                          </div>
            
                                </div>
                                
                            </div>
                        </div>
                    </div>
</div></div></div></div>
        </section>
                    <!-- Content Row -->
                    
            <!-- End of Main Content -->
            <br><hr>
          <div class="card text-center">
  <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs">
      <li class="nav-item">
        <a class="nav-link active" aria-current="true" href="https://github.com/PRIYANKA-Nigam">Github</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://twitter.com/PriyankaN2504">Twitter</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://linkedin.com/in/priyanka-nigam-083247172">LinkedIn</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="https://facebook.com/priyanka.nigam.9256">Facebook</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://hackerrank.com/ec_1634831038">Hackerrank</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://gitlab.com/dashboard/projects">Gitlab</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://stackoverflow.com/users/14454060/priyanka-nigam">Stack Overflow</a>
      </li>
    </ul>
  </div>
  <div class="card-body">
    <h5 class="card-title">Our Collaboration </h5>
    <p class="card-text">
      <p><strong>Thank you for visiting our site. </strong></p>
      Please do also follow us on the above sites.Your 1 follow 
      can help us reach to greater heights.We have good number of contents in the above sites with a awesome personal 
      portfolio.All the documents are earned with efforts and none are forged or bought from other sources.
      Kindly, check our page.It will boost our morale to give more exciting content for you and will also be 
      helpful for you to make understanding easier for you with demonstrative live examples.
    </p>
    <a href="#" class="btn btn-primary">Go Top</a>
  </div>
</div>
           
      </div>
     <a style="padding:10px;display:block;" href="<?= base_url().'Welcome/Blog'?>" target="_blank"></a>

  <a href="<?= base_url().'Welcome/test3'?>" class="float">
<i class="fas fa-info-circle"></i>
</a> 
      <footer class="sticky-footer bg-white">
      <div class="container my-auto">
          <div class="copyright text-center my-auto">
              <span>Copyright &copy; Your Website 2023</span>
          </div>
      </div>
    
  </footer> 
  
          <script src="https://translate.google.com/translate_a/element.js?cb=loadGoogleTranslate"></script>
        <script>
           function loadGoogleTranslate(){
            new google.translate.TranslateElement("google_element");
           }
            </script>
             <script> 
     var icon=document.getElementById("icon");
     icon.onclick =function(){
      document.body.classList.toggle("dark-theme");
      if(document.body.classList.contains("dark-theme")){
        icon.src="<?=base_url().'setting.jpg'?>";
      }else{
        icon.src="<?=base_url().'moon.jpg'?>";
      }
     }
     </script>      
</body>
</html>        