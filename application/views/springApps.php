
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Spring Apps</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" 
    integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" 
    crossorigin="anonymous" referrerpolicy="no-referrer" />

      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" 
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
        <style> 
        body{
	font-family:Verdana, Geneva, sans-serif;
	font-size:18px;
	background-color:#CCC;
}

.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.fas{
	margin-top:18px;
	font-size: 2em;
} 
 body{
  background: var(--primary-color);
  max-height: 100vh; 
  /* by giving max-height: 100vh the footer got stick to the bottom of the page */
}
</style> 
        </head>
    <body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" 
    crossorigin="anonymous"></script>
    <div class="row">
    <div id="google_element" style="float:right"></div>
  <script src="https://translate.google.com/translate_a/element.js?cb=loadGoogleTranslate"></script>
        <script>
           function loadGoogleTranslate(){
            new google.translate.TranslateElement("google_element");
           }
            </script>
    <a href="<?=base_url().'Welcome/Tutorials'?>" ><button class="btn btn-success" >Back</button></a> 
    <div class="col-md-12" style="background-color: black;color:white">
   <h1><b>Spring API Tutorials</b></h1> 
   <?php
//   echo '<pre>';
//   var_dump($result);
//   echo '</pre>';
//   die;
      foreach($result as $key=>$value){
            // print_r($value);
            ?>
   <div class="col-md-6">  
       
       <li class="list-group-item">
       
         <?php
           $data=$value['url'];
           $final=str_replace('watch?v=','embed/',$data);
           echo "
           <iframe src='$final' 
           frameborder='0'
           allow='autoplay:encrypted-media'
           allowfullscreen>
           </iframe>
           ";
           ?></li> <?=$value['title']?></div>
            
            <?php
          }
        ?>
    </div>
    
    <div class="card-body">
                           <div class="album py-5 bg-light">
                              <div class="container">
                                 <div class="row">
                                    <h1>Spring Boot APIs</h1>
                                 <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <h4 class="card-text"><b>1.</b></h4>
              <div class="d-flex justify-content-between align-items-center">
                              <div class="container">
                                <div class="row">
                                <h3> Storing JSP form data to h2 in-memory database</h3>
                                <a href="<?= base_url().'Welcome/springPDF?name=api_using_jsp_jpa_h2_in_memory_db.pdf'?>" ><button class="btn btn-primary" style="margin-left: 30px;margin-top:15px;">View</button></a>
                                </div>
                              </div>
                         </div></div></div></div>
            <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <p class="card-text"><b>2.</b></p>
              <div class="d-flex justify-content-between align-items-center">
              <div class="container">
                                <div class="row">
                               <h3>Fetch stored data from h2 in-memory database using jpa repository</h3>
                                <a href="<?= base_url().'Welcome/springPDF?name=get_api_using_jdbcTemplate_h2_in_memory_db-merged.pdf'?>" ><button class="btn btn-primary" style="margin-left: 30px;margin-top:15px;">View</button></a>
                                </div>
                              </div>
                                    </div> </div></div></div>
                                    <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <p class="card-text"><b>3.</b></p>
              <div class="d-flex justify-content-between align-items-center">
              <div class="container">
                                <div class="row">
                               <h3> Store data to h2 in-memory database using jdbctemplate </h3>
                                <a href="<?= base_url().'Welcome/springPDF?name=get_api_using_jdbcTemplate_h2_in_memory_db.pdf'?>" ><button class="btn btn-primary" style="margin-left: 30px;margin-top:15px;">View</button></a>
                                </div>
                              </div>
                                    
                         </div></div></div></div>
            <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <p class="card-text"><b>4.</b></p>
              <div class="d-flex justify-content-between align-items-center">
              <div class="container">
                                <div class="row">
                                <h3>CRUD operation with postman using ResponseEntity class</h3>
                                <a href="<?= base_url().'Welcome/springPDF?name=post_get_api_with_responseEntity_class.pdf'?>" ><button class="btn btn-primary" style="margin-left: 30px;margin-top:15px;">View</button></a>
                                </div>
                              </div>
                                    </div> </div></div></div>
                                    <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <div class="card-body">
              <p class="card-text"><b>5.</b></p>
              <div class="d-flex justify-content-between align-items-center">
              <div class="container">
                                <div class="row">
                                <h3>Implementation of basic GET API </h3>
                                <a href="<?= base_url().'Welcome/springPDF?name=get_api_response.pdf'?>" ><button class="btn btn-primary" style="margin-left: 30px;margin-top:15px;">View</button></a>
                                </div>
                              </div>
                                    </div> </div></div></div>
                                   
                                 </div></div></div></div>



    </div>
    <?php 
        include('footer.php');
        ?>
         <a href="<?= base_url().'Welcome/test3'?>" class="float">
<i class="fas fa-info-circle"></i>
</a>
    </body>
</html>