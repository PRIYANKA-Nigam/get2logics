<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" 
    integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" 
    crossorigin="anonymous" referrerpolicy="no-referrer" />
  <style>
   *{padding:0;margin:0;}

body{
	font-family:Verdana, Geneva, sans-serif;
	font-size:18px;
	background-color:#CCC;
}

.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.fas{
	margin-top:18px;
	font-size: 2em;
} 
body{
    margin: 0;padding: 0;
    font-family: Arial, Helvetica, sans-serif;
}
#header{
    width: 100%;
    height: 56px;
    background-color: black;
}
#menu{
    width: 1024px;
    min-height: 500px;
    margin: 0px auto;
    color: white;
}
h2{
    margin: 0px;
}
#wrapper{
    width: 1024px;
    min-height: 500px;
    margin: 0px auto;
}
#portfolio{
    list-style-type: none;
    padding-left: 0px;
}
#portfolio li{
    display: inline-block;
    margin: 10px 5px 10px 5px;
}
#portfolio li img{
    width: 250px;
    height: 250px;
}
#portfolio li img:hover{
    cursor: pointer;
}
#overlay:hover{
    cursor: pointer;
}
#main:hover{
    cursor: pointer;
}
#frame{
    display: none;
    background-color: white;
    left: 50%;
    width: 700px;
    height: 430px;
    position: fixed;
    margin-left: -350px;
    margin-top: 0px;
    padding-top: 0px;
    padding-bottom: 20px;
}
/* replacing #frame img with #main everywhere */
#main{
    width: 660px;
    height: 600px;
    margin-left: 20px;
}
#overlay{
    display: none;
    width: 100%;
    height: 100%;
    position: fixed;
    background-color: black;
    opacity: 0.7;
}
#frame-table{
    position: absolute;
    width: 660px;
    margin-left: 20px;
}
#right{
    float: right;
}
#left img{
    margin-left: -3px;
}
#right img{
    margin-right: -3px;
}
#left,#right{
    position: relative;
    top: 250px;
    opacity: 0.7;
}
#desc{
    background-color: white;
    padding-bottom: 10px;
}
#desc p{
    margin-left: 20px;
}
#filter li{
    display: inline;
}
#filter li.active{
    text-decoration: underline;
}



  </style>

    <title>JQuery image Portfolio</title>
</head>
<body>
    <div id="header">
        <div id="menu">
            <h2>jQuery Portfolio</h2>
            <input type="text" placeholder="search..." style="float:right" id="search" />
        </div>
    </div>
    <div id="overlay"></div>
    <div id="frame">
        <table id="frame-table">
            <tr>
                <td id="left">
              <img src="<?=base_url()?>assets/images/le.png" alt="Left" width="80px" height="80px" />
                </td>
                <td id="right">
               <img src="<?=base_url()?>assets/images/ri.png" alt="Right" width="80px" height="80px"/>
                </td>
            </tr>
        </table>
        <img id="main" src="" alt=""/>
        <div id="desc">
        <p></p>
          </div>  

    </div>
     <a href="<?=base_url().'Welcome/Blog'?>" ><button class="btn btn-success" style="float:right">Back</button></a>
    <div id="wrapper">
        <ul id="filter">
            <h1><div id="aa" style="background:red;width:80%;height:50px;display:block;"><center><b><i>
                <u>C/C++</u></i></b></center></div></h1>
            <li class="active">all</li> ||
            <li>mem_layout</li> ||
            <li>keywords</li> ||
            <li>datatype</li> ||
            <li>loop</li> ||
            <li>array_vectors_list</li> ||
            <li>pointer</li> ||
            <li>snippets</li> ||
            <li>operators</li> ||
            <li>sorting</li> ||
            <li>recursion</li> ||
            <li>pointer</li> ||
            <li>linker_loader</li> ||
            <li>c_c++</li> ||
            <li>function</li> ||
            <li>constructor_dest</li> ||
            <li>identifiers</li> ||
            <li>containers</li> ||
            <li>basics</li> ||
            <li>general</li> 
        </ul>
        <ul id="portfolio">
            <?php include_once("listImg.php")  ?>
        </ul>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" 
    crossorigin="anonymous"></script>
   
    <script>
        $(function(){
            var current_li;
            $("#search").keyup(function(){
            var current_query = $("#search").val();
            if(current_query!=""){
                $("#portfolio li").hide();  
            $("#portfolio li").each(function(){
             var current_keyword = $(this).attr("data-keywords");
             if(current_keyword.indexOf(current_query)>=0){
                $(this).show();
             } //with keypress() the searchable item is shown but on removing that item from search bar 
            });//it doesn't show back all the items.so,replacing keypress() with keyup()
            }else{
                $("#portfolio li").show();
            }
         
            });
            function setImg(src,id){
               // $("#frame img").attr("src",src); or
              $("#main").attr("src",src);
            //   var path="text/"+id+".txt";
            //   $.get(path,function(data){
            //   // console.log(data);
            //    $("#desc p").html(data);
            //   });
            }
$("#portfolio img").click(function(){
var src=$(this).attr("src");
var id= $(this).attr("id");
current_li=$(this).parent();
setImg(src,id);
$("#frame").fadeIn();
$("#overlay").fadeIn();
});
// jquery reduces a lot of code lines and if we right the same code using js it will occupy lots many line of codes
$("#overlay").click(function(){
 $(this).fadeOut();
 $("#frame").fadeOut();
    });
    $("#right").click(function(){
        if(current_li.is(":last-child")){
        var next_li= $("#portfolio li").first();
        }else{
            var next_li=current_li.next();
        }
      var next_li=current_li.next();
      var next_src= next_li.children("img").attr("src");
      var id= next_li.children("img").attr("id");
    //   $("#main").attr("src",next_src);
      setImg(next_src,id);
      current_li=next_li;
    });
    $("#left").click(function(){
        if(current_li.is(":first-child")){
        var prev_li= $("#portfolio li").last();
        }else{
            var prev_li=current_li.prev();
        }
     var prev_li = current_li.prev();
     var prev_src =prev_li.children("img").attr("src");
     var id =prev_li.children("img").attr("id");
    //  $("#main").attr("src",prev_src);
     setImg(prev_src,id);
     current_li=prev_li;
});
$("#right,#left").mouseover(function(){
$(this).css("opacity","0.75");
});
$("#right,#left").mouseleave(function(){
$(this).css("opacity","0.55");
});
});
///////////////////////////another jquery /////////////////////////////
$(function(){
$("#filter li").click(function(){
var category = $(this).html();
$("#aa").html(category);//The html() method sets or returns the content (innerHTML) of the selected elements.
$("#filter li").removeClass("active");
$(this).addClass("active");
// $("#portfolio li").fadeOut(); //on clicking on any category all images will disappear
//or
$("#portfolio li").hide();
$("#portfolio li").each(function(){
    if($(this).hasClass(category)){
        // $(this).fadeIn(); or
        $(this).show();
    }
// var test = $(this).html();
});

});
});
    </script>
     <a href="<?= base_url().'Welcome/test3'?>" class="float">
<i class="fas fa-info-circle"></i>
</a>    
</body>
</html>